import jenkins

#login
serverUrl = "http://127.0.0.1:8080"
username = "admin"
password = "admin"
server = jenkins.Jenkins(serverUrl,username,password)

defProjectName = "test-devops-service"
newProjectName = "test-devops-service02"


if server.job_exists(newProjectName) != True :
    print("项目不存在开始新建项目")
    
    config_xml=server.get_job_config(defProjectName)
    newconfig_xml = config_xml.replace("<defaultValue>svn</defaultValue>","<defaultValue>git</defaultValue>")
    
    print(newconfig_xml)

    server.create_job(newProjectName,newconfig_xml)
else:
    print("项目已存在!")