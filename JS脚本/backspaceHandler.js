/*
 *解决：页面中的焦点处于非编辑框时，点击backspace按钮 回退到login页面的问题
 *by wangzhhr@chanjet.com
 */
// window.history.back(0); // 禁用当前页面后退按钮
window.onkeydown = function(e) {
	if (e.keyCode == 8) { //Backspace按键：不执行浏览器的回退事件
		// debugger;
		var src = e.relatedTarget || e.srcElement || e.target || e.currentTarget;
		var tagName = src.nodeName ? src.nodeName.toLowerCase() : '' //标签名称
		var tagType = src.type ? src.type.toLowerCase() : ''; //标签类型
		if (tagName == "textarea" ||
		 tagName == "input" && src.readOnly == false && tagType != "submit" && tagType != "button" && tagType != "image" && tagType != "checkbox" && tagType != "radio")
			return;
		if (e.preventDefault) {
			e.preventDefault();
		}
		if (e.returnValue) {
			e.returnValue = false;
		}
		return false;
	}
}