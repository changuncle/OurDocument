USE [AntifakeSign]
GO
/****** Object:  View [dbo].[V_PersonalAccount_AlterEmailAddress]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_PersonalAccount_AlterEmailAddress]
AS
SELECT   dbo.PersonalAccount_AlterEmailAddress.Id, dbo.PersonalAccount_AlterEmailAddress.UserCode, 
                dbo.PersonalAccount_AlterEmailAddress.OldEmailName, dbo.PersonalAccount_AlterEmailAddress.NewEmailName, 
                dbo.PersonalAccount_AlterEmailAddress.ProxyMan, dbo.PersonalAccount_AlterEmailAddress.SimpleDisplayName, 
                dbo.PersonalAccount_AlterEmailAddress.FirstDeptCode, dbo.PersonalAccount_AlterEmailAddress.SecondDeptCode, 
                dbo.PersonalAccount_AlterEmailAddress.ThirdDeptCode, dbo.PersonalAccount_AlterEmailAddress.DeptName, 
                dbo.PersonalAccount_AlterEmailAddress.DeptManager, dbo.PersonalAccount_AlterEmailAddress.IsSync, 
                dbo.PersonalAccount_AlterEmailAddress.CreateDate, dbo.PersonalAccount_AlterEmailAddress.Remarks, 
                dbo.PersonalAccount_AlterEmailAddress.FlowNo, dbo.PersonalAccount_AlterEmailAddress.SAMAccountName, 
                dbo.PersonalAccount_AlterEmailAddress.DeptId, dbo.Sys_Department.Dept1Name AS FirstDeptName, 
                dbo.Sys_Department.Dept2Name AS SecondDeptName
FROM      dbo.PersonalAccount_AlterEmailAddress INNER JOIN
                dbo.Sys_Department ON dbo.PersonalAccount_AlterEmailAddress.DeptId = dbo.Sys_Department.DeptId

GO
/****** Object:  View [dbo].[V_PersonalAccount_Apply]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE VIEW [dbo].[V_PersonalAccount_Apply]
AS
SELECT   dbo.PersonalAccount_Apply.AccountId, dbo.PersonalAccount_Apply.UserPinyin, dbo.PersonalAccount_Apply.UserCode, 
                dbo.PersonalAccount_Apply.UserType, dbo.PersonalAccount_Apply.UserPwd, dbo.PersonalAccount_Apply.WorkPlace, 
                dbo.PersonalAccount_Apply.FirstDeptCode, dbo.PersonalAccount_Apply.SecondDeptCode, 
                dbo.PersonalAccount_Apply.ThirdDeptCode, dbo.PersonalAccount_Apply.DeptName, 
                dbo.PersonalAccount_Apply.DeptManager, dbo.PersonalAccount_Apply.AccountExpireDate, 
                dbo.PersonalAccount_Apply.ProxyMan, dbo.PersonalAccount_Apply.CreateDate, dbo.PersonalAccount_Apply.Deleter, 
                dbo.PersonalAccount_Apply.DeleteDate, dbo.PersonalAccount_Apply.IsDeleted, dbo.PersonalAccount_Apply.Remarks, 
                dbo.PersonalAccount_Apply.FlowNo, d.DictName AS WorkPlaceName,'0' as Status, '' as CheckMan, 
                dbo.PersonalAccount_Apply.DeptId, dbo.Sys_Department.Dept1Name AS FirstDeptName, 
                dbo.Sys_Department.Dept2Name AS SecondDeptName
FROM      dbo.PersonalAccount_Apply INNER JOIN
                dbo.Sys_Dictionary AS d ON dbo.PersonalAccount_Apply.WorkPlace = d.DictCode INNER JOIN 
                dbo.Sys_Department ON dbo.PersonalAccount_Apply.DeptId = dbo.Sys_Department.DeptId
 
--union

--SELECT   dbo.PersonalAccount_Apply.AccountId, dbo.PersonalAccount_Apply.UserPinyin, dbo.PersonalAccount_Apply.UserCode, 
--                dbo.PersonalAccount_Apply.UserType, dbo.PersonalAccount_Apply.UserPwd, dbo.PersonalAccount_Apply.WorkPlace, 
--                dbo.PersonalAccount_Apply.FirstDeptCode, dbo.PersonalAccount_Apply.SecondDeptCode, 
--                dbo.PersonalAccount_Apply.ThirdDeptCode, dbo.PersonalAccount_Apply.DeptName, 
--                dbo.PersonalAccount_Apply.DeptManager, dbo.PersonalAccount_Apply.AccountExpireDate, 
--                dbo.PersonalAccount_Apply.ProxyMan, dbo.PersonalAccount_Apply.CreateDate, dbo.PersonalAccount_Apply.Deleter, 
--                dbo.PersonalAccount_Apply.DeleteDate, dbo.PersonalAccount_Apply.IsDeleted, dbo.PersonalAccount_Apply.Remarks, 
--                dbo.PersonalAccount_Apply.FlowNo, d.DictName AS WorkPlaceName, CASE WHEN t2.WorkflowNodeCode ='N02_DeptManagerApprove' THEN 104101 ELSE 104100 END AS Status, RIGHT(t4.CheckPinyinCode,LEN(t4.CheckPinyinCode)-CHARINDEX(' ',t4.CheckPinyinCode
--)) CheckMan, 
--                dbo.PersonalAccount_Apply.DeptId, dbo.Sys_Department.Dept1Name AS FirstDeptName, 
--                dbo.Sys_Department.Dept2Name AS SecondDeptName
--FROM      dbo.PersonalAccount_Apply INNER JOIN
--                dbo.Sys_Dictionary AS d ON dbo.PersonalAccount_Apply.WorkPlace = d.DictCode 
--				--INNER JOIN
--    --                (SELECT   WorkflowTypeId, Status, CheckMan
--    --                 FROM      dbo.PersonalAccount_Workflow
--    --                 WHERE   (WorkflowType = 101100100)) AS e ON 
--    --            dbo.PersonalAccount_Apply.AccountId = e.WorkflowTypeId 
--				inner join [dbo].[WorkflowInstance] t2 on AccountId= t2.WorkflowTypeId
--				inner join WorkFlowTask t4 on t2.ID=t4.WorkflowInstanceID and t4.State='active'
--				INNER JOIN
--                dbo.Sys_Department ON dbo.PersonalAccount_Apply.DeptId = dbo.Sys_Department.DeptId


GO
/****** Object:  View [dbo].[V_PersonalAccount_Apply_UserList]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_PersonalAccount_Apply_UserList]
AS
SELECT   dbo.PersonalAccount_Apply_UserList.Id, dbo.PersonalAccount_Apply_UserList.AccountId, 
                dbo.PersonalAccount_Apply_UserList.UserType, dbo.PersonalAccount_Apply_UserList.UserCode, 
                dbo.PersonalAccount_Apply_UserList.UserPrefix, dbo.PersonalAccount_Apply_UserList.UserPinyin, 
                dbo.PersonalAccount_Apply_UserList.UserPwd, dbo.PersonalAccount_Apply_UserList.OpenMail, 
                dbo.PersonalAccount_Apply_UserList.OpenLync, dbo.PersonalAccount_Apply_UserList.OpenProxy, 
                dbo.PersonalAccount_Apply_UserList.IsCreateNotesId, dbo.PersonalAccount_Apply_UserList.IsAcceptOutsideMail, 
                dbo.PersonalAccount_Apply_UserList.IsSendOutsideMail, dbo.PersonalAccount_Apply_UserList.IsSync, 
                dbo.PersonalAccount_Apply_UserList.SAMAccountName, dbo.PersonalAccount_Apply_UserList.EmailAddress, 
                dbo.PersonalAccount_Apply_UserList.FirstName, dbo.PersonalAccount_Apply_UserList.LastName, 
                dbo.PersonalAccount_Apply_UserList.OrderFlag, dbo.PersonalAccount_Apply_UserList.CreateDate, 
                dbo.Sys_Dictionary.DictName AS UserTypeName, d.Status, d.CheckMan, 
                dbo.PersonalAccount_Apply_UserList.AccountStatus
FROM      dbo.PersonalAccount_Apply_UserList INNER JOIN
                dbo.Sys_Dictionary ON dbo.PersonalAccount_Apply_UserList.UserType = dbo.Sys_Dictionary.DictCode INNER JOIN
                dbo.PersonalAccount_Apply ON 
                dbo.PersonalAccount_Apply_UserList.AccountId = dbo.PersonalAccount_Apply.AccountId INNER JOIN
                    (SELECT   WorkflowTypeId, Status, CheckMan
                     FROM      dbo.PersonalAccount_Workflow
                     WHERE   (WorkflowType = 101100100)) AS d ON dbo.PersonalAccount_Apply.AccountId = d.WorkflowTypeId

union

SELECT   dbo.PersonalAccount_Apply_UserList.Id, dbo.PersonalAccount_Apply_UserList.AccountId, 
                dbo.PersonalAccount_Apply_UserList.UserType, dbo.PersonalAccount_Apply_UserList.UserCode, 
                dbo.PersonalAccount_Apply_UserList.UserPrefix, dbo.PersonalAccount_Apply_UserList.UserPinyin, 
                dbo.PersonalAccount_Apply_UserList.UserPwd, dbo.PersonalAccount_Apply_UserList.OpenMail, 
                dbo.PersonalAccount_Apply_UserList.OpenLync, dbo.PersonalAccount_Apply_UserList.OpenProxy, 
                dbo.PersonalAccount_Apply_UserList.IsCreateNotesId, dbo.PersonalAccount_Apply_UserList.IsAcceptOutsideMail, 
                dbo.PersonalAccount_Apply_UserList.IsSendOutsideMail, dbo.PersonalAccount_Apply_UserList.IsSync, 
                dbo.PersonalAccount_Apply_UserList.SAMAccountName, dbo.PersonalAccount_Apply_UserList.EmailAddress, 
                dbo.PersonalAccount_Apply_UserList.FirstName, dbo.PersonalAccount_Apply_UserList.LastName, 
                dbo.PersonalAccount_Apply_UserList.OrderFlag, dbo.PersonalAccount_Apply_UserList.CreateDate, 
                dbo.Sys_Dictionary.DictName AS UserTypeName,CASE WHEN t2.WorkflowNodeCode ='N02_DeptManagerApprove' THEN 104101 ELSE 104100 END AS Status, RIGHT(t4.CheckPinyinCode,LEN(t4.CheckPinyinCode)-CHARINDEX(' ',t4.CheckPinyinCode)) CheckMan, 
                dbo.PersonalAccount_Apply_UserList.AccountStatus
FROM      dbo.PersonalAccount_Apply_UserList INNER JOIN
                dbo.Sys_Dictionary ON dbo.PersonalAccount_Apply_UserList.UserType = dbo.Sys_Dictionary.DictCode 
				INNER JOIN
                dbo.PersonalAccount_Apply ON 
                dbo.PersonalAccount_Apply_UserList.AccountId = dbo.PersonalAccount_Apply.AccountId 
				inner join [dbo].[WorkflowInstance] t2 on PersonalAccount_Apply.AccountId= t2.WorkflowTypeId
				inner join WorkFlowTask t4 on t2.ID=t4.WorkflowInstanceID and t4.State='active'

GO
/****** Object:  View [dbo].[V_PersonalAccount_Close]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[V_PersonalAccount_Close]
AS
				select-- wf.WorkflowId, WorkflowTypeId,wf.WorkflowTempId,wf.CurrentStep, Status, CheckMan,
				pac.*,
				dept.Dept1Name as FirstDeptName,dept.Dept2Name as SecondDeptName  
				  from PersonalAccount_Close  pac left join  [Sys_Department]  dept
				on pac.ThirdDeptCode=dept.Dept3Code
				--left join PersonalAccount_Workflow wf on wf.WorkflowTypeId=pac.Id 
				 

GO
/****** Object:  View [dbo].[V_PersonalAccount_Close_UserList_forIndex]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
 --管理员手动处理任务查询视图
CREATE View [dbo].[V_PersonalAccount_Close_UserList_forIndex] as

with V_T as (
 SELECT   cul.Id as WorkflowId, pawfC.WorkflowName, pawfC.WorkflowType, cul.Id  as WorkflowTypeId, pawfC.WorkflowTempId, 
                pawfC.Status, pawfC.CurrentStep, cul.AdminName as CheckMantemp, pawfC.CheckManType, pawfC.Creator, 
                pawfC.CreateDate, b.DeptName, sdA.DictName AS CheckManTypeName, sdB.DictName AS StatusName, 
                '/PersonalAccount/PersonalAccount_Close/Approve' AS AuditUrl, '/PersonalAccount/PersonalAccount_Close/Detail' AS DetailUrl
				FROM    dbo.PersonalAccount_Workflow pawfC INNER JOIN
                dbo.PersonalAccount_Close AS b ON pawfC.WorkflowTypeId = b.Id INNER JOIN
                dbo.Sys_Dictionary AS sdA ON pawfC.CheckManType = sdA.DictCode INNER JOIN
                dbo.Sys_Dictionary AS sdB ON pawfC.Status = sdB.DictCode
				inner join PersonalAccount_Close_UserList  cul on cul.AccountId=pawfC.WorkflowTypeId where cul.IsSync=0 and cul.DeleteFlag=0 and cul.AdminName is not null  and   Status=104101 
			 ) 
			 SELECT WorkflowId,WorkflowName, WorkflowType, WorkflowTypeId, WorkflowTempId, Status, CurrentStep,  CheckManType, Creator,   CreateDate, DeptName, 
			 CheckManTypeName, StatusName, AuditUrl, DetailUrl ,b.x.value('.','varchar(100)') AS CheckPinyinCode FROM(
SELECT * ,phase=CAST('<items><item>'+REPLACE(CheckMantemp,',','</item><item>')+'</item></items>' AS xml) from V_T 

) c CROSS apply c.phase.nodes('//item') AS b (x)



GO
/****** Object:  View [dbo].[V_PublicAccount_Apply]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_PublicAccount_Apply]
AS
SELECT   dbo.PublicAccount_Apply.AccountId, dbo.PublicAccount_Apply.UserPinyin, dbo.PublicAccount_Apply.UserCode, 
                dbo.PublicAccount_Apply.UserType, dbo.PublicAccount_Apply.UserPwd, dbo.PublicAccount_Apply.Users, 
                dbo.PublicAccount_Apply.WorkPlace, dbo.PublicAccount_Apply.FirstDeptCode, 
                dbo.PublicAccount_Apply.SecondDeptCode, dbo.PublicAccount_Apply.ThirdDeptCode, 
                dbo.PublicAccount_Apply.DeptName, dbo.PublicAccount_Apply.DeptManager, 
                dbo.PublicAccount_Apply.SAMAccountName, dbo.PublicAccount_Apply.AccountExpireDate, dbo.PublicAccount_Apply.Tel, 
                dbo.PublicAccount_Apply.EmailAddress, dbo.PublicAccount_Apply.ProxyMan, dbo.PublicAccount_Apply.Charger, 
                dbo.PublicAccount_Apply.OpenMail, dbo.PublicAccount_Apply.OpenLync, dbo.PublicAccount_Apply.IsSync, 
                dbo.PublicAccount_Apply.CreateDate, dbo.PublicAccount_Apply.Remarks, dbo.PublicAccount_Apply.FlowNo, 
                c.DictName AS UserTypeName, dbo.PublicAccount_Apply.DeptId, dbo.Sys_Department.Dept1Name AS FirstDeptName, 
                dbo.Sys_Department.Dept2Name AS SecondDeptName, dbo.PublicAccount_Apply.RealCharger, 
                dbo.PublicAccount_Apply.CnValue, dbo.PublicAccount_Apply.AccountStatus --dbo.PublicAccount_Workflow.Status, 
FROM      dbo.PublicAccount_Apply INNER JOIN
                dbo.Sys_Dictionary AS c ON dbo.PublicAccount_Apply.UserType = c.DictCode INNER JOIN
                dbo.Sys_Department ON dbo.PublicAccount_Apply.DeptId = dbo.Sys_Department.DeptId --INNER JOIN
                --dbo.PublicAccount_Workflow ON dbo.PublicAccount_Apply.AccountId = dbo.PublicAccount_Workflow.WorkflowTypeId


GO
/****** Object:  View [dbo].[V_Sys_DeptWorkflow]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Sys_DeptWorkflow]
AS
SELECT   dbo.Sys_DeptWorkflow.Id, dbo.Sys_DeptWorkflow.Dept1Code, dbo.Sys_DeptWorkflow.Dept1Name, 
                dbo.Sys_DeptWorkflow.Dept2Code, dbo.Sys_DeptWorkflow.Dept2Name, dbo.Sys_DeptWorkflow.Dept3Code, 
                dbo.Sys_DeptWorkflow.Dept3Name, dbo.Sys_DeptWorkflow.WorkflowType, dbo.Sys_DeptWorkflow.WorkflowId, 
                dbo.Sys_DeptWorkflow.Remarks, dbo.Sys_DeptWorkflow.Creator, dbo.Sys_DeptWorkflow.CreateDate, 
                dbo.Workflow.WorkflowName AS WorkflowIdName, dbo.Sys_Dictionary.DictName AS WorkflowTypeName
FROM      dbo.Sys_DeptWorkflow INNER JOIN
                dbo.Workflow ON dbo.Sys_DeptWorkflow.WorkflowId = dbo.Workflow.WorkflowId INNER JOIN
                dbo.Sys_Dictionary ON dbo.Sys_DeptWorkflow.WorkflowType = dbo.Sys_Dictionary.DictCode

GO
/****** Object:  View [dbo].[V_Sys_UserRole]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Sys_UserRole]
AS
SELECT   t1.Id, t1.UserCode, t1.RoleCode, t1.Creator, t1.CreateDate, t2.Name AS PinyinCode
FROM      dbo.Sys_User_Role AS t1 INNER JOIN
                dbo.Sys_User AS t2 ON t1.UserCode = t2.Code

GO
/****** Object:  View [dbo].[V_Sys_UsertypeOpenPermission]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Sys_UsertypeOpenPermission]
AS
SELECT   TOP (100) PERCENT dbo.Sys_UsertypeOpenPermission.Id, dbo.Sys_UsertypeOpenPermission.UserType, 
                dbo.Sys_UsertypeOpenPermission.PersonalAccountOpenPermission, dbo.Sys_UsertypeOpenPermission.Creator, 
                dbo.Sys_UsertypeOpenPermission.CreateDate, b.DictName AS UserTypeName, 
                c.DictName AS PersonalAccountOpenPermissionName
FROM      dbo.Sys_UsertypeOpenPermission INNER JOIN
                dbo.Sys_Dictionary AS b ON dbo.Sys_UsertypeOpenPermission.UserType = b.DictCode INNER JOIN
                dbo.Sys_Dictionary AS c ON dbo.Sys_UsertypeOpenPermission.PersonalAccountOpenPermission = c.DictCode

GO
/****** Object:  View [dbo].[V_Sys_UsertypeProxyVPN]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Sys_UsertypeProxyVPN]
AS
SELECT   dbo.Sys_UsertypeProxyVPN.Id, dbo.Sys_UsertypeProxyVPN.UserType, dbo.Sys_UsertypeProxyVPN.Proxy, 
                dbo.Sys_UsertypeProxyVPN.VPN, dbo.Sys_UsertypeProxyVPN.Creator, dbo.Sys_UsertypeProxyVPN.CreateDate, 
                dbo.Sys_Dictionary.DictName AS UserTypeName, dbo.Sys_UsertypeProxyVPN.DefaultGroup
FROM      dbo.Sys_Dictionary INNER JOIN
                dbo.Sys_UsertypeProxyVPN ON dbo.Sys_Dictionary.DictCode = dbo.Sys_UsertypeProxyVPN.UserType

GO
/****** Object:  View [dbo].[V_WelcomeAllSubmitList]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*流程实例（包含待审批人信息）*/
CREATE VIEW [dbo].[V_WelcomeAllSubmitList]
AS
SELECT DISTINCT 
                WT.TaskId, WI.Id AS WorkflowInstanceId, V.WorkflowTypeId, WI.WorkflowCode, WF.WorkflowName, 
                WI.WorkflowNodeCode, WN.WorkflowNodeName, WT.CheckPinyinCode AS Approver, V.ProxyMan, V.CreateDate, 
                V.DeptName, V.AuditUrl, V.SubmitUrl
FROM      (SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_Apply/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_AlterEmailAddress/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_AlterEmailAddress/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_AlterEmailAddress AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_ResetPwd AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PublicAccount/PublicAccount_Apply/Audit' AS AuditUrl, 
                                 '/PublicAccount/PublicAccount_Apply/Create' AS SubmitUrl
                 FROM      dbo.PublicAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Close/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_Close/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_Close AS T1
                 WHERE   (IsDeleted = 0)) AS V INNER JOIN
                dbo.WorkflowInstance AS WI ON V.WorkflowTypeId = WI.WorkflowTypeId AND WI.IsDeleted = 0 LEFT OUTER JOIN
                dbo.Workflow AS WF ON WI.WorkflowCode = WF.WorkflowCode AND WF.IsDeleted = 0 LEFT OUTER JOIN
                dbo.WorkflowNode AS WN ON WI.WorkflowCode = WN.WorkFlowCode AND 
                WI.WorkflowNodeCode = WN.WorkflowNodeCode AND WN.IsDeleted = 0 LEFT OUTER JOIN
                    (SELECT   TaskId, WorkflowInstanceId, WorkflowNodeCode, CheckPinyinCode
                     FROM      dbo.WorkflowTask
                     WHERE   (State = 'active') AND (IsDeleted = 0) OR
                                     (RIGHT(WorkflowNodeCode, 9) = '_Finished') OR
                                     (RIGHT(WorkflowNodeCode, 7) = '_Closed')) AS WT ON WI.Id = WT.WorkflowInstanceId AND 
                WI.WorkflowNodeCode = WT.WorkflowNodeCode

GO
/****** Object:  View [dbo].[V_WelcomeOnceHandleList]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*流程实例曾经处理过的记录（包括当前待处理人）*/
CREATE VIEW [dbo].[V_WelcomeOnceHandleList]
AS
SELECT DISTINCT 
                WT.TaskId, WI.Id AS WorkflowInstanceId, V.WorkflowTypeId, WI.WorkflowCode, WF.WorkflowName, 
                WI.WorkflowNodeCode, WN.WorkflowNodeName, WR.Approver, RIGHT(ISNULL(WT.CheckPinyinCode, ''), 
                LEN(ISNULL(WT.CheckPinyinCode, '')) - CHARINDEX(' ', ISNULL(WT.CheckPinyinCode, ''))) AS EmpCode, V.ProxyMan, 
                V.CreateDate, V.DeptName, V.AuditUrl
FROM      (SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_AlterEmailAddress AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_ResetPwd AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PublicAccount/PublicAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PublicAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Close/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_Close AS T1
                 WHERE   (IsDeleted = 0)) AS V INNER JOIN
                dbo.WorkflowInstance AS WI ON V.WorkflowTypeId = WI.WorkflowTypeId AND WI.IsDeleted = 0 INNER JOIN
                    (SELECT   TaskId, WorkflowInstanceId, WorkflowNodeCode, CheckPinyinCode
                     FROM      dbo.WorkflowTask
                     WHERE   (State = 'inactive') AND (IsDeleted = 0)) AS WT ON WI.Id = WT.WorkflowInstanceId INNER JOIN
                dbo.WorkflowRecord AS WR ON WT.TaskId = WR.WorkflowTaskId LEFT OUTER JOIN
                dbo.WorkflowNode AS WN ON WI.WorkflowCode = WN.WorkFlowCode AND 
                WI.WorkflowNodeCode = WN.WorkflowNodeCode AND WN.IsDeleted = 0 LEFT OUTER JOIN
                dbo.Workflow AS WF ON WI.WorkflowCode = WF.WorkflowCode AND WF.IsDeleted = 0

GO
/****** Object:  View [dbo].[V_WelcomeOnceHandleList_back]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_WelcomeOnceHandleList_back]
AS
/*流程实例曾经处理过的记录（包括当前待处理人）*/ SELECT DISTINCT 
                WI.ID AS WorkflowInstanceId, V.WorkflowTypeId, WI.WorkflowCode, WF.WorkflowName, WI.WorkflowNodeCode, 
                WN.WorkflowNodeName, WR.Approver, RIGHT(ISNULL(WT.CheckPinyinCode, ''), 
                LEN(ISNULL(WT.CheckPinyinCode, '')) - CHARINDEX(' ', ISNULL(WT.CheckPinyinCode, ''))) AS EmpCode, V.ProxyMan, 
                V.CreateDate, V.DeptName, V.AuditUrl
FROM      (SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_AlterEmailAddress AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_ResetPwd AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PublicAccount/PublicAccount_Apply/Audit' AS AuditUrl
                 FROM      dbo.PublicAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Close/Audit' AS AuditUrl
                 FROM      dbo.PersonalAccount_Close AS T1
                 WHERE   (IsDeleted = 0)) AS V INNER JOIN
                dbo.WorkflowInstance AS WI ON V.WorkflowTypeId = WI.WorkflowTypeID AND WI.IsDeleted = 0 INNER JOIN
                dbo.WorkflowTask AS WT ON WI.ID = WT.WorkflowInstanceID INNER JOIN
                dbo.WorkflowRecord AS WR ON WT.TaskId = WR.WorkflowTaskId LEFT JOIN
                    (SELECT   WorkflowInstanceId, WorkflowNodeCode, [Approver] = stuff
                                         ((SELECT   ',' + [CheckPinyinCode]
                                           FROM      WorkflowTask t
                                           WHERE   t .State = 'active' AND t .IsDeleted = 0 AND 
                                                           t .WorkflowInstanceId = WorkflowTask.WorkflowInstanceId AND 
                                                           t .WorkflowNodeCode = WorkflowTask.WorkflowNodeCode FOR xml path('')), 1, 1, '')
                     FROM      WorkflowTask
                     WHERE   State = 'active' AND IsDeleted = 0
                     GROUP BY WorkflowInstanceId, WorkflowNodeCode) WFT ON WI.ID = WFT.WorkflowInstanceID LEFT OUTER JOIN
                dbo.WorkflowNode AS WN ON WI.WorkflowCode = WN.WorkFlowCode AND 
                WI.WorkflowNodeCode = WN.WorkflowNodeCode AND WN.IsDeleted = 0 LEFT OUTER JOIN
                dbo.Workflow AS WF ON WI.WorkflowCode = WF.WorkflowCode AND WF.IsDeleted = 0

GO
/****** Object:  View [dbo].[V_WelcomeUnderCheckingList]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_WelcomeUnderCheckingList]
AS
SELECT   WT.TaskId, V.WorkflowTypeId, WT.WorkflowInstanceId, WF.WorkflowId, WF.WorkflowCode, WF.WorkflowName, 
                WI.WorkflowNodeCode, WN.WorkflowNodeName AS CheckManTypeName, WT.CheckPinyinCode, 
                RIGHT(ISNULL(WT.CheckPinyinCode, ''), LEN(ISNULL(WT.CheckPinyinCode, '')) - CHARINDEX(' ', 
                ISNULL(WT.CheckPinyinCode, ''))) AS EmpCode, V.ProxyMan AS Creator, V.CreateDate, V.DeptName, 
                CASE WHEN WI.WorkflowNodeCode LIKE 'N__/_ReSubmitted' ESCAPE 
                '/' THEN 'resubmit' WHEN RIGHT(WI.WorkflowNodeCode, 5) 
                = 'Audit' THEN 'approving' WHEN WI.WorkflowNodeCode LIKE 'N__/_Finished' ESCAPE 
                '/' THEN 'finish' WHEN WI.WorkflowNodeCode LIKE 'N__/_Closed' ESCAPE '/' THEN 'close' ELSE '' END AS FlowStatus, 
                V.AuditUrl, V.SubmitUrl, WN.Isbatch
FROM      (SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Apply/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_Apply/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_AlterEmailAddress/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_AlterEmailAddress/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_AlterEmailAddress AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_ResetPwd/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_ResetPwd AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   AccountId AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PublicAccount/PublicAccount_Apply/Audit' AS AuditUrl, 
                                 '/PublicAccount/PublicAccount_Apply/Create' AS SubmitUrl
                 FROM      dbo.PublicAccount_Apply AS T1
                 WHERE   (IsDeleted = 0)
                 UNION
                 SELECT   Id AS WorkflowTypeId, ProxyMan, CreateDate, DeptName, 
                                 '/PersonalAccount/PersonalAccount_Close/Audit' AS AuditUrl, 
                                 '/PersonalAccount/PersonalAccount_Close/Create' AS SubmitUrl
                 FROM      dbo.PersonalAccount_Close AS T1
                 WHERE   (IsDeleted = 0)) AS V INNER JOIN
                dbo.WorkflowInstance AS WI ON V.WorkflowTypeId = WI.WorkflowTypeId AND WI.IsDeleted = 0 INNER JOIN
                dbo.WorkflowTask AS WT ON WI.Id = WT.WorkflowInstanceId AND WT.State = 'active' LEFT OUTER JOIN
                dbo.WorkflowNode AS WN ON WI.WorkflowCode = WN.WorkFlowCode AND 
                WI.WorkflowNodeCode = WN.WorkflowNodeCode LEFT OUTER JOIN
                dbo.Workflow AS WF ON WI.WorkflowCode = WF.WorkflowCode AND WF.IsDeleted = 0

GO
/****** Object:  View [dbo].[V_WorkflowRecord]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_WorkflowRecord]
AS
SELECT   WI.Id AS WorkflowInstanceId, WI.WorkflowTypeId, WI.WorkflowCode, WI.WorkflowNodeCode, WN.WorkflowNodeName, 
                WN.RoleCode, WN.RoleDesc, WR.WorkflowRecordId, WT.TaskId AS WorkflowTaskId, WR.WorkflowTransitionCode, 
                WTR.Name AS WorkflowTransitionName, WR.Approver, WR.ApprovedDate, WR.Remarks, WR.CreateDate, 
                WR.Creator
FROM      dbo.WorkflowRecord AS WR INNER JOIN
                dbo.WorkflowTask AS WT ON WR.WorkflowTaskId = WT.TaskId INNER JOIN
                dbo.WorkflowInstance AS WI ON WT.WorkflowInstanceId = WI.Id INNER JOIN
                dbo.WorkflowNode AS WN ON WT.WorkflowNodeCode = WN.WorkflowNodeCode AND 
                WN.WorkFlowCode = WI.WorkflowCode LEFT OUTER JOIN
                dbo.WorkflowTransition AS WTR ON WR.WorkflowTransitionCode = WTR.Code AND 
                WTR.WorkflowCode = WI.WorkflowCode
WHERE   (WR.IsDeleted = 0)

GO
/****** Object:  View [dbo].[V_WorkflowTask]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*根据实体id查询当前流程处理人*/
CREATE VIEW [dbo].[V_WorkflowTask]
AS
SELECT   inst.WorkflowCode, inst.WorkflowTypeId, task.TaskId, task.WorkflowInstanceId, task.WorkflowNodeCode, 
                task.CheckPinyinCode, task.State, task.IsDeleted, task.Creator, task.CreateDate, task.Modifier, task.ModifyDate, 
                task.Deleter, task.DeleteDate, task.Remarks
FROM      dbo.WorkflowInstance AS inst LEFT OUTER JOIN
                dbo.WorkflowTask AS task ON inst.Id = task.WorkflowInstanceId
WHERE   (task.State = 'active') OR
                (RIGHT(inst.WorkflowNodeCode, 9) = '_Finished') OR
                (RIGHT(inst.WorkflowNodeCode, 7) = '_Closed')

GO
/****** Object:  View [dbo].[V_WorkflowTransition]    Script Date: 2020/1/9 19:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_WorkflowTransition]
AS
SELECT DISTINCT 
                t1.Id, t1.WorkflowCode, t4.WorkflowName, t1.Name, t1.Code, t1.FromCode, t2.WorkflowNodeName AS FromNodeName, 
                t2.WorkflowNodeType AS FromNodeType, t1.ToCode, t3.WorkflowNodeName AS ToNodeName, 
                t3.WorkflowNodeType AS ToNodeType
FROM      dbo.WorkflowTransition AS t1 INNER JOIN
                dbo.Workflow AS t4 ON t1.WorkflowCode = t4.WorkflowCode INNER JOIN
                dbo.WorkflowNode AS t2 ON t1.FromCode = t2.WorkflowNodeCode AND t1.WorkflowCode = t2.WorkFlowCode INNER JOIN
                dbo.WorkflowNode AS t3 ON t1.ToCode = t3.WorkflowNodeCode AND t1.WorkflowCode = t3.WorkFlowCode
WHERE   (t1.IsDeleted = 0) AND (t2.IsDeleted = 0) AND (t3.IsDeleted = 0) AND (t4.IsDeleted = 0)

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PersonalAccount_AlterEmailAddress"
            Begin Extent = 
               Top = 17
               Left = 347
               Bottom = 289
               Right = 552
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Department"
            Begin Extent = 
               Top = 130
               Left = 83
               Bottom = 323
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_AlterEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_AlterEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[15] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PersonalAccount_Apply"
            Begin Extent = 
               Top = 21
               Left = 477
               Bottom = 314
               Right = 675
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 226
               Left = 706
               Bottom = 402
               Right = 863
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 21
               Left = 754
               Bottom = 141
               Right = 939
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Department"
            Begin Extent = 
               Top = 134
               Left = 189
               Bottom = 310
               Right = 359
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 23
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PersonalAccount_Apply_UserList"
            Begin Extent = 
               Top = 83
               Left = 781
               Bottom = 321
               Right = 989
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Dictionary"
            Begin Extent = 
               Top = 65
               Left = 1184
               Bottom = 268
               Right = 1341
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PersonalAccount_Apply"
            Begin Extent = 
               Top = 102
               Left = 431
               Bottom = 334
               Right = 629
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 101
               Left = 136
               Bottom = 221
               Right = 321
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 3765
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply_UserList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply_UserList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PersonalAccount_Apply_UserList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[17] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -39
      End
      Begin Tables = 
         Begin Table = "PublicAccount_Apply"
            Begin Extent = 
               Top = 24
               Left = 368
               Bottom = 314
               Right = 566
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 23
               Left = 667
               Bottom = 199
               Right = 824
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Department"
            Begin Extent = 
               Top = 158
               Left = 81
               Bottom = 353
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PublicAccount_Workflow"
            Begin Extent = 
               Top = 220
               Left = 595
               Bottom = 445
               Right = 785
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 3810
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Widt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PublicAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'h = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PublicAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PublicAccount_Apply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[61] 4[11] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Sys_DeptWorkflow"
            Begin Extent = 
               Top = 4
               Left = 573
               Bottom = 306
               Right = 746
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Dictionary"
            Begin Extent = 
               Top = 81
               Left = 827
               Bottom = 281
               Right = 984
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Workflow"
            Begin Extent = 
               Top = 157
               Left = 83
               Bottom = 337
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1755
         Width = 1995
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_DeptWorkflow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_DeptWorkflow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 45
               Left = 362
               Bottom = 237
               Right = 516
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 46
               Left = 43
               Bottom = 186
               Right = 225
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1605
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3150
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UserRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UserRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Sys_UsertypeOpenPermission"
            Begin Extent = 
               Top = 58
               Left = 216
               Bottom = 234
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 125
               Left = 28
               Bottom = 306
               Right = 185
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 38
               Left = 568
               Bottom = 223
               Right = 725
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UsertypeOpenPermission'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UsertypeOpenPermission'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Sys_Dictionary"
            Begin Extent = 
               Top = 7
               Left = 570
               Bottom = 246
               Right = 727
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_UsertypeProxyVPN"
            Begin Extent = 
               Top = 46
               Left = 231
               Bottom = 233
               Right = 385
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2880
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UsertypeProxyVPN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Sys_UsertypeProxyVPN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WI"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 146
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WF"
            Begin Extent = 
               Top = 6
               Left = 1006
               Bottom = 146
               Right = 1201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WN"
            Begin Extent = 
               Top = 6
               Left = 755
               Bottom = 146
               Right = 968
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WT"
            Begin Extent = 
               Top = 6
               Left = 508
               Bottom = 146
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeAllSubmitList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeAllSubmitList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WI"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 146
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WT"
            Begin Extent = 
               Top = 6
               Left = 508
               Bottom = 149
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WR"
            Begin Extent = 
               Top = 6
               Left = 755
               Bottom = 146
               Right = 988
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WN"
            Begin Extent = 
               Top = 6
               Left = 1026
               Bottom = 146
               Right = 1239
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WF"
            Begin Extent = 
               Top = 150
               Left = 38
               Bottom = 290
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeOnceHandleList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeOnceHandleList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeOnceHandleList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeOnceHandleList_back'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeOnceHandleList_back'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "WI"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 162
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WT"
            Begin Extent = 
               Top = 6
               Left = 508
               Bottom = 220
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WN"
            Begin Extent = 
               Top = 6
               Left = 755
               Bottom = 146
               Right = 968
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WF"
            Begin Extent = 
               Top = 6
               Left = 1006
               Bottom = 144
               Right = 1201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2340
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 150' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeUnderCheckingList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeUnderCheckingList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WelcomeUnderCheckingList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "WR"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WT"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 146
               Right = 518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WI"
            Begin Extent = 
               Top = 6
               Left = 556
               Bottom = 146
               Right = 765
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WN"
            Begin Extent = 
               Top = 6
               Left = 803
               Bottom = 146
               Right = 1016
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WTR"
            Begin Extent = 
               Top = 6
               Left = 1054
               Bottom = 146
               Right = 1230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowRecord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowRecord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowRecord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "inst"
            Begin Extent = 
               Top = 34
               Left = 59
               Bottom = 212
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "task"
            Begin Extent = 
               Top = 15
               Left = 542
               Bottom = 341
               Right = 751
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowTask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowTask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[59] 4[2] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 57
               Left = 392
               Bottom = 289
               Right = 568
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t4"
            Begin Extent = 
               Top = 58
               Left = 80
               Bottom = 253
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 287
               Left = 150
               Bottom = 501
               Right = 363
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 37
               Left = 660
               Bottom = 290
               Right = 873
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowTransition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_WorkflowTransition'
GO
