
--更新Sys_Menu一级大类
declare @oldPrefix varchar(10),@newPrefix varchar(10)
set @oldPrefix='106'
set @newPrefix='105'
update Sys_Menu set MenuCode=@newPrefix,ParentCode='0',OrderNo=@newPrefix 
where MenuCode=@oldPrefix
update Sys_Menu set MenuCode=@newPrefix+SUBSTRING(MenuCode,4,9),ParentCode=@newPrefix+SUBSTRING(ParentCode,4,9),OrderNo=@newPrefix+SUBSTRING(MenuCode,4,9) 
where SUBSTRING(ParentCode,1,3)=@oldPrefix

--更新Sys_Menu二级子类
declare @oldPrefix varchar(10),@newPrefix varchar(10)
set @oldPrefix='105116'
set @newPrefix='105111'
update Sys_Menu set MenuCode=@newPrefix,OrderNo=@newPrefix 
where MenuCode=@oldPrefix
update Sys_Menu set MenuCode=@newPrefix+SUBSTRING(MenuCode,7,9),ParentCode=@newPrefix+SUBSTRING(ParentCode,7,9),OrderNo=@newPrefix+SUBSTRING(MenuCode,7,9) 
where SUBSTRING(ParentCode,1,6)=@oldPrefix