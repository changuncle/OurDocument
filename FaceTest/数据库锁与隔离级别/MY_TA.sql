USE [NHDemo]
GO
/****** Object:  Table [dbo].[MY_TA]    Script Date: 2020/5/28 20:17:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MY_TA](
	[id] [int] NOT NULL,
	[NAME] [varchar](10) NULL,
	[Phone] [nchar](11) NULL,
	[Age] [int] NULL,
 CONSTRAINT [PK_MY_TA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (1, N'A''AA', N'11111111111', 101)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (2, N'B%BB', N'22222222222', 102)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (3, N'C[CC', N'33333333333', 103)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (4, N'D]DD', N'44444444444', 104)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (5, N'E^EE', N'55555555555', 105)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (6, N'F_FF', N'66666666666', 106)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (7, N'FFFF', N'77777777777', 107)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (8, N'B9BB', N'88888888888', 108)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (9, N'GGG', N'99999999999', 109)
INSERT [dbo].[MY_TA] ([id], [NAME], [Phone], [Age]) VALUES (10, N'HHH', N'00000000000', 110)
