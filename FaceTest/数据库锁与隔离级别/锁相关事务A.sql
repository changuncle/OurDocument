
--查看数据库快照状态
select name,user_access,user_access_desc,snapshot_isolation_state,snapshot_isolation_state_desc,is_read_committed_snapshot_on
from sys.databases
--开启SNAPSHOT
--ALTER DATABASE NHDemo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
--ALTER DATABASE NHDemo SET ALLOW_SNAPSHOT_ISOLATION ON
--ALTER DATABASE NHDemo SET READ_COMMITTED_SNAPSHOT ON
--ALTER DATABASE NHDemo SET MULTI_USER
--关闭SNAPSHOT
--ALTER DATABASE NHDemo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
--ALTER DATABASE NHDemo SET ALLOW_SNAPSHOT_ISOLATION OFF
--ALTER DATABASE NHDemo SET READ_COMMITTED_SNAPSHOT OFF
--ALTER DATABASE NHDemo SET MULTI_USER

--DBCC USEROPTIONS;
set transaction isolation level Read UnCommitted
set transaction isolation level Read Committed
set transaction isolation level Repeatable Read
set transaction isolation level Serializable

--nolock、readpast、holdlock、updlock、xlock
--rowlock、paglock、tablock、tablockx

begin tran tran1
--select * from MY_TA with(nolock) where Id='2'
--select * from MY_TA with(readpast) where Id='2'
--select * from MY_TA with(holdlock) where Id='2'
--select * from MY_TA with(updlock) where Id='2'
--select * from MY_TA with(xlock) where Id='2'
--update MY_TA set NAME='B%BBnew' where Id='2'
--update MY_TA set NAME='B%BBnew' where Age='102'
--update MY_TA set NAME='B%BBnew' where Age='102' and id=2
--delete from MY_TA where id=2
--delete from MY_TA where Name='B%BB'
--delete from MY_TA where id=2 and Name='B%BB'
insert into MY_TA values(11,'Ext01','Ext11111111',201)

rollback tran tran1
commit tran tran1

select * from sys.dm_tran_locks where resource_database_id=DB_ID('NHDemo')
