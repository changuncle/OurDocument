
--查看数据库快照状态
select name,user_access,user_access_desc,snapshot_isolation_state,snapshot_isolation_state_desc,is_read_committed_snapshot_on
from sys.databases
--开启SNAPSHOT
--ALTER DATABASE NHDemo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
--ALTER DATABASE NHDemo SET ALLOW_SNAPSHOT_ISOLATION ON
--ALTER DATABASE NHDemo SET READ_COMMITTED_SNAPSHOT ON
--ALTER DATABASE NHDemo SET MULTI_USER
--关闭SNAPSHOT
--ALTER DATABASE NHDemo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
--ALTER DATABASE NHDemo SET ALLOW_SNAPSHOT_ISOLATION OFF
--ALTER DATABASE NHDemo SET READ_COMMITTED_SNAPSHOT OFF
--ALTER DATABASE NHDemo SET MULTI_USER

--DBCC USEROPTIONS;
set transaction isolation level Read UNCommitted
set transaction isolation level Read Committed
set transaction isolation level Repeatable Read
set transaction isolation level serializable

select * from MY_TA where Id='2'
select * from MY_TA where Phone='22222222222'
select * from MY_TA where Id='3'
select * from MY_TA where Name='C[CC'
select * from MY_TA with(REPEATABLEread) where Id='3'
select * from MY_TA with(readcommitted)where NAME='C[CC'

begin tran tran2
--select * from MY_TA
--select * from MY_TA where Id=2
--select * from MY_TA where Phone='22222222222'
--select * from MY_TA where id=3
select * from MY_TA where Phone='33333333333'
--select * from MY_TA where Age=103
--select * from MY_TA where Age=103 and Phone='33333333333' and id=3
--select * from MY_TA where Age=103 and Phone='33333333333' and id!=2
--select * from MY_TA where Age=103 and Phone='33333333333' and id!=11
--select * from MY_TA where id!=4
select * from MY_TA where Age=103 and Phone='33333333333' and id!=4

rollback tran tran2

select * from sys.dm_tran_locks where resource_database_id=DB_ID('NHDemo')