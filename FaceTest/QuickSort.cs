using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    public class QuickSort
    {
        public static void Run(int[] array, int low, int high)
        {
            if (low < high)
            {
                int middleIndex = GetMiddleIndex(array, low, high);
                Run(array, low, middleIndex - 1);
                Run(array, middleIndex + 1, high);
            }
        }

        public static int GetMiddleIndex(int[] array, int low, int high)
        {
            int middleIndex = low;
            int middleValue = array[middleIndex];
            while (low < high)
            {
                while (low < high && middleValue <= array[high])//从右往左找比middleValue小的值
                {
                    high--;
                }
                if (low < high)//array[high]<middleValue跳出while
                {
                    array[middleIndex] = array[high];
                    array[high] = middleValue;
                    middleIndex = high;
                }
                while (low < high && middleValue >= array[low])//从左往右找比currentValue大的值
                {
                    low++;
                }
                if (low < high)//array[low]>middleValue跳出while
                {
                    array[middleIndex] = array[low];
                    array[low] = middleValue;
                    middleIndex = low;
                }
            }
            return middleIndex;
        }
    }
}
