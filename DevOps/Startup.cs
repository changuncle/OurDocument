﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Audit.SqlServer.Providers;
using H3C.TS.Business;
using H3C.TS.Business.Interface;
using H3C.TS.Common;
using H3C.TS.Web.Controllers.Filters;
using H3C.TS.Web.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;

namespace H3C.TS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;

            //读取appsettings.json中的配置项
            new AppsettingsUtility().Initial(configuration);
            //处理SqlMap.xml
            ReadSQLXml.Init();

            //[配置NLog（1）]
            env.ConfigureNLog("nlog.config");
            LogManager.Configuration.Variables["connectionString"] = Configuration["ConnectionString:SqlServer:Value"];
            LogManager.Configuration.Variables["configDir"] = Configuration.GetSection("LogFilesDir").Value;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //[Controller中使用Session（1）]添加Session
            services.AddDistributedMemoryCache();
            services.AddSession(o => { o.IdleTimeout = TimeSpan.FromMinutes(30); });

            //非Controller使用Session（1）
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //注释掉以下6行内容后可以跨Controller读写Session
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //添加全局过滤器
            services.AddMvc(options => { options.Filters.Add(new CheckSessionFilter() { IsCheck = true }); });

            services.AddCors(corsOptions =>
            {
                corsOptions.AddPolicy("AllowCors", builder => builder.WithOrigins(AppsettingsUtility.AllowCors.Origins)
                                                                     .WithHeaders(AppsettingsUtility.AllowCors.Headers)
                                                                     .WithMethods(AppsettingsUtility.AllowCors.Methods)
                                                                     .AllowCredentials());
            });
            services.AddSingleton<Microsoft.AspNetCore.Mvc.Infrastructure.IActionContextAccessor, Microsoft.AspNetCore.Mvc.Infrastructure.ActionContextAccessor>();
            //添加IConfiguration注入
            //services.AddSingleton<IConfiguration>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                SupportedUICultures = new[] { new CultureInfo("zh-CN") }
            });
            app.UseCors("AllowCors");

            //[配置NLog（2）]
            loggerFactory.AddNLog();
            app.AddNLogWeb();

            //配置Audit日志记录方式
            Audit.Core.Configuration.DataProvider = new SqlDataProvider()
            {
                ConnectionString = AppsettingsUtility.ConnectionString.SqlServer.Value,
                Schema = "dbo",
                TableName = "Sys_AuditLog",
                IdColumnName = "Id",
                JsonColumnName = "Data",
                LastUpdatedDateColumnName = "CreateDate"
            };

            app.UseStaticFiles();
            app.UseCookiePolicy();

            //[Controller中使用Session（2）]启用Session
            app.UseSession();

            //非Controller使用Session（2）
            app.UseStaticHttpContext();

            //注入TestInject（2）
            app.UseMiddleware<WebCaching>();

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasAuthorityGroup",
                  areaName: "AuthorityGroup",
                  template: "AuthorityGroup/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasDomainGroup",
                  areaName: "DomainGroup",
                  template: "DomainGroup/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasPCDM",
                  areaName: "PCDM",
                  template: "PCDM/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasPersonalAccount",
                  areaName: "PersonalAccount",
                  template: "PersonalAccount/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasPersonalAuthority",
                  areaName: "PersonalAuthority",
                  template: "PersonalAuthority/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasPublicAccount",
                  areaName: "PublicAccount",
                  template: "PublicAccount/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasSync",
                  areaName: "Sync",
                  template: "Sync/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasSys",
                  areaName: "Sys",
                  template: "Sys/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasWorkflow",
                  areaName: "Workflow",
                  template: "Workflow/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });
            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasAgent",
                  areaName: "Agent",
                  template: "Agent/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });
            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                  name: "areasPushamil",
                  areaName: "Pushmail",
                  template: "Pushmail/{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Index" }
                );
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Login", action = "Index" });
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default2",
                    template: "{controller}/{action}/{id}/{parm}",
                    defaults: new { controller = "TokenRoute", action = "index" });
            });
        }
    }
}
