USE [ADDB]
GO

/****** Object:  Table [dbo].[t_app_WorkFlow]    Script Date: 2019/6/30 21:20:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_app_WorkFlow](
	[WorkflowID] [varchar](8000) NOT NULL,
	[WorkflowName] [varchar](8000) NULL,
	[WorkflowTypeID] [varchar](8000) NULL,
	[WorkflowType] [varchar](8000) NULL,
	[WorkflowState] [varchar](8000) NULL,
	[CDate] [datetime] NULL,
	[ApproveStatus] [varchar](8000) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

