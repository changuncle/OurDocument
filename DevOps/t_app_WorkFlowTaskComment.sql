USE [ADDB]
GO

/****** Object:  Table [dbo].[t_app_WorkFlowTaskComment]    Script Date: 2019/6/30 21:20:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_app_WorkFlowTaskComment](
	[ID] [varchar](100) NOT NULL,
	[TaskID] [varchar](8000) NULL,
	[WorkFlowID] [varchar](8000) NULL,
	[ApproveMan] [varchar](8000) NULL,
	[Comment] [varchar](8000) NULL,
	[CDate] [datetime] NULL,
	[Result] [varchar](8000) NULL,
 CONSTRAINT [PK_t_App_WorkFlowTaskComment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

