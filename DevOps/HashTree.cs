﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreConsoleApp01.Common
{
    public class HashTree
    {
        /// <summary>
        /// 获取二叉树结构
        /// </summary>
        /// <returns></returns>
        public static TreeNode GetTreeNode()
        {
            TreeNode root = new TreeNode(8);
            TreeNode node11 = new TreeNode(4);
            TreeNode node12 = new TreeNode(12);
            TreeNode node21 = new TreeNode(2);
            TreeNode node22 = new TreeNode(6);
            TreeNode node23 = new TreeNode(10);
            TreeNode node24 = new TreeNode(14);
            TreeNode node31 = new TreeNode(1);
            TreeNode node32 = new TreeNode(3);
            TreeNode node33 = new TreeNode(5);
            TreeNode node34 = new TreeNode(11);
            TreeNode node35 = new TreeNode(13);
            TreeNode node36 = new TreeNode(15);

            node21.leftChild = node31;
            node21.rightChild = node32;
            node22.leftChild = node33;
            node23.rightChild = node34;
            node24.leftChild = node35;
            node24.rightChild = node36;
            node11.leftChild = node21;
            node11.rightChild = node22;
            node12.leftChild = node23;
            node12.rightChild = node24;
            root.leftChild = node11;
            root.rightChild = node12;
            return root;
        }

        /// <summary>
        /// 递归实现前序排列（上级->左->右）
        /// </summary>
        /// <param name="node"></param>
        public static void PreTraverseTreeByRecursive(TreeNode node)
        {
            if (node == null) return;
            Console.Write(node.value + " ");
            PreTraverseTreeByRecursive(node.leftChild);
            PreTraverseTreeByRecursive(node.rightChild);
        }

        /// <summary>
        /// 递归实现中序排列（左->上级->右）
        /// </summary>
        /// <param name="node"></param>
        public static void MidTraverseTreeByRecursiveMinToMax(TreeNode node)
        {
            if (node == null) return;
            MidTraverseTreeByRecursiveMinToMax(node.leftChild);
            Console.Write(node.value + " ");
            MidTraverseTreeByRecursiveMinToMax(node.rightChild);
        }

        /// <summary>
        /// 递归实现中序排列（右->上级->左）
        /// </summary>
        /// <param name="node"></param>
        public static void MidTraverseTreeByRecursiveMaxToMin(TreeNode node)
        {
            if (node == null) return;
            MidTraverseTreeByRecursiveMaxToMin(node.rightChild);
            Console.Write(node.value + " ");
            MidTraverseTreeByRecursiveMaxToMin(node.leftChild);
        }

        /// <summary>
        /// 递归实现后序排列（左->右->上级）
        /// </summary>
        /// <param name="node"></param>
        public static void PostTraverseTreeByRecursive(TreeNode node)
        {
            if (node == null) return;
            PostTraverseTreeByRecursive(node.leftChild);
            PostTraverseTreeByRecursive(node.rightChild);
            Console.Write(node.value + " ");
        }

        /// <summary>
        /// 栈实现前序排列（上级->左->右）
        /// </summary>
        /// <param name="node"></param>
        public static void PreTraverseTreeByStack(TreeNode root)
        {
            Stack<TreeNode> stack = new Stack<TreeNode>();
            TreeNode treeNode = root;
            while (treeNode != null || stack.Count > 0)
            {
                while (treeNode != null)
                {
                    //本节点处理完毕后再入栈，左孩子处理完毕后再把左孩子入栈，弹栈后再处理右孩子，这样保证了“上级->左->右”的处理顺序
                    Console.Write(treeNode.value + " ");
                    stack.Push(treeNode);
                    treeNode = treeNode.leftChild;
                }
                if (stack.Count > 0)
                {
                    TreeNode temp = stack.Pop();
                    treeNode = temp.rightChild;
                }
            }
        }

        /// <summary>
        /// 栈实现中序排列（左->上级->右）
        /// </summary>
        /// <param name="node"></param>
        public static void MidTraverseTreeByStack(TreeNode root)
        {
            Stack<TreeNode> stack = new Stack<TreeNode>();
            TreeNode treeNode = root;
            while (treeNode != null || stack.Count > 0)
            {
                while (treeNode != null)
                {
                    stack.Push(treeNode);
                    treeNode = treeNode.leftChild;
                }
                if (stack.Count > 0)
                {
                    TreeNode temp = stack.Pop();
                    //本节点处理完毕后再把右孩子入栈，这样保证了“上级->右”的处理顺序
                    Console.Write(temp.value + " ");
                    treeNode = temp.rightChild;
                }
            }
        }

        /// <summary>
        /// 栈实现后序排列（左->右->上级）
        /// </summary>
        /// <param name="node"></param>
        public static void PostTraverseTreeByStack(TreeNode root)
        {
            Stack<TreeNode> stack = new Stack<TreeNode>();
            TreeNode treeNode = root;
            TreeNode lastVisit = null;
            while (treeNode != null || stack.Count > 0)
            {
                while (treeNode != null)
                {
                    stack.Push(treeNode);
                    treeNode = treeNode.leftChild;
                }
                if (stack.Count > 0)
                {
                    TreeNode temp = stack.Pop();
                    if (temp.rightChild == null || temp.rightChild == lastVisit)
                    {
                        //栈本身存储的是左孩子，左孩子处理完毕后，判断上级节点没右孩子/右孩子已处理才能处理上级节点，这样保证了“左->右->上级”的处理顺序
                        Console.Write(temp.value + " ");
                        treeNode = null;
                        lastVisit = temp;
                    }
                    else
                    {
                        //若本节点的右孩子不为空且未处理，就先把本节点入栈再把右孩子入栈，这样保证了“右->上级”的处理顺序
                        stack.Push(temp);
                        treeNode = temp.rightChild;
                    }
                }
            }
        }

        /// <summary>
        /// 队列实现层级排列
        /// </summary>
        /// <param name="root"></param>
        public static void LevelTraverseTreeByQueue(TreeNode root)
        {
            Queue<TreeNode> queue = new Queue<TreeNode>();
            TreeNode treeNode = root;
            queue.Enqueue(treeNode);
            while (queue.Count > 0)
            {
                treeNode = queue.Dequeue();
                //处理完本节点后把本节点的孩子加入队列，这样就能保证孩子在上层节点之后
                Console.Write(treeNode.value + " ");
                if (treeNode.leftChild != null)
                {
                    queue.Enqueue(treeNode.leftChild);
                }
                if (treeNode.rightChild != null)
                {
                    queue.Enqueue(treeNode.rightChild);
                }
            }
        }
    }
}
