USE [ADDB]
GO

/****** Object:  Table [dbo].[t_app_WorkFlowTask]    Script Date: 2019/6/30 21:20:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_app_WorkFlowTask](
	[ID] [varchar](50) NOT NULL,
	[Name] [varchar](8000) NULL,
	[WorkflowID] [varchar](8000) NULL,
	[ApproveMan] [varchar](8000) NULL,
	[Step] [varchar](8000) NULL,
	[State] [varchar](8000) NULL,
	[CDate] [datetime] NULL,
	[ApplyType] [varchar](8000) NULL,
 CONSTRAINT [PK_t_App_WorkFlowTask] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对应t_app_WorkFlow中的主键WorkflowID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_app_WorkFlowTask', @level2type=N'COLUMN',@level2name=N'WorkflowID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前审批人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_app_WorkFlowTask', @level2type=N'COLUMN',@level2name=N'ApproveMan'
GO

