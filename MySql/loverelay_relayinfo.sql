CREATE DATABASE  IF NOT EXISTS `loverelay` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `loverelay`;
-- MySQL dump 10.13  Distrib 8.0.40, for Win64 (x86_64)
--
-- Host: localhost    Database: loverelay
-- ------------------------------------------------------
-- Server version	9.1.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `relayinfo`
--

DROP TABLE IF EXISTS `relayinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `relayinfo` (
  `Id` varchar(36) NOT NULL,
  `RelayType` int NOT NULL,
  `Title` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `BoardingDate` datetime DEFAULT NULL,
  `TrainNo` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `CarriageNo` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `SeatNo` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `OnceMinutes` int DEFAULT NULL,
  `Times` int DEFAULT NULL,
  `ProviderId` varchar(50) DEFAULT NULL,
  `ProviderIcon` varchar(100) DEFAULT NULL,
  `SeekerId` varchar(50) DEFAULT NULL,
  `SeekerIcon` varchar(100) DEFAULT NULL,
  `PairedTime` datetime DEFAULT NULL,
  `RewardAmount` float DEFAULT NULL,
  `RelayStatus` int NOT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `CreatedTime` datetime DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relayinfo`
--

LOCK TABLES `relayinfo` WRITE;
/*!40000 ALTER TABLE `relayinfo` DISABLE KEYS */;
INSERT INTO `relayinfo` VALUES ('fa0a53ea-94dc-11ef-9d2d-7e3660000001',0,'第一个爱心接力','2024-12-12 14:54:38','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,NULL,NULL,NULL,'2024-12-20 10:48:59'),('fa0a53ea-94dc-11ef-9d2d-7e3660000002',0,'第2个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:29:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000003',0,'第3个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:28:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000004',0,'第4个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:27:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000005',0,'第5个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:26:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000006',1,'第6个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 14:25:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000007',1,'第7个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:24:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000008',1,'第8个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 15:23:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000009',1,'第9个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 14:22:38',NULL),('fa0a53ea-94dc-11ef-9d2d-7e3660000010',1,'第10个爱心接力','2024-12-12 00:00:00','z207','116','37',15,1,'providerId01','string','seekerId01','string','2024-12-12 14:54:38',0,0,'guo',NULL,'2024-12-12 14:21:38',NULL);
/*!40000 ALTER TABLE `relayinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-12-28 18:22:58
