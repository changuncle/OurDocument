
use loverelay;

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '中国香港', 89687, '/pages/dashboard/index', 
'影视明星真的多，人人都爱大中国', '影视明星真的多，人人都爱大中国', 
'/static/images/province/xiangGang.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '中国澳门', 88687, '/pages/dashboard/index', 
'异域风情它最行，一朵莲花向五星', '异域风情它最行，一朵莲花向五星', 
'/static/images/province/aoMen.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '北京市', 87687, '/pages/dashboard/index', 
'紫禁城里看华夏，中华文明耀万家', '紫禁城里看华夏，中华文明耀万家', 
'/static/images/province/beiJing.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '天津市', 86687, '/pages/dashboard/index', 
'说话好比讲相声，麻花包子嘴里扔', '说话好比讲相声，麻花包子嘴里扔', 
'/static/images/province/tianJin.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '上海市', 85687, '/pages/dashboard/index', 
'东方明珠亮晶晶，科技发展永不停', '东方明珠亮晶晶，科技发展永不停', 
'/static/images/province/shangHai.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '重庆市', 84687, '/pages/dashboard/index', 
'地铁横穿大楼房，火锅底料真的香', '地铁横穿大楼房，火锅底料真的香', 
'/static/images/province/chongQing.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '内蒙古自治区', 83687, '/pages/dashboard/index', 
'风吹草低见牛羊，成吉思汗名声响', '风吹草低见牛羊，成吉思汗名声响', 
'/static/images/province/neiMengGu.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '广西壮族自治区', 82687, '/pages/dashboard/index', 
'桂林山水甲天下，螺蛳粉被人人夸', '桂林山水甲天下，螺蛳粉被人人夸', 
'/static/images/province/guangXi.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '新疆维吾尔自治区', 81687, '/pages/dashboard/index', 
'瓜果河流都不少，丝绸之路很重要', '瓜果河流都不少，丝绸之路很重要', 
'/static/images/province/xinJiang.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '宁夏回族自治区', 80687, '/pages/dashboard/index', 
'枸杞影城和羊羔，塞上江南景色好', '枸杞影城和羊羔，塞上江南景色好', 
'/static/images/province/ningXia.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '西藏自治区', 79687, '/pages/dashboard/index', 
'青藏高原作屋脊，布达拉宫很神秘', '青藏高原作屋脊，布达拉宫很神秘', 
'/static/images/province/xiZang.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '河北省', 78687, '/pages/dashboard/index', 
'华北明珠白洋淀，芦苇荡内白鹳鸣', '华北明珠白洋淀，芦苇荡内白鹳鸣', 
'/static/images/province/heBei.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '山西省', 77687, '/pages/dashboard/index', 
'五台山内寺庙多，太原陈醋别错过', '五台山内寺庙多，太原陈醋别错过', 
'/static/images/province/shanXiPingYangGuLou.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '辽宁省', 76687, '/pages/dashboard/index', 
'沈阳故宫很低调，盘锦大米真的妙', '沈阳故宫很低调，盘锦大米真的妙', 
'/static/images/province/liaoNing.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '吉林省', 75687, '/pages/dashboard/index', 
'长白山上白云摇，土特产竟是中药', '长白山上白云摇，土特产竟是中药', 
'/static/images/province/jiLin.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '黑龙江省', 74687, '/pages/dashboard/index', 
'千里冰封白雪飘，吃着冰棍看冰雕', '千里冰封白雪飘，吃着冰棍看冰雕', 
'/static/images/province/heiLongJiang.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '江苏省', 73687, '/pages/dashboard/index', 
'秦淮两岸风光好，苏州园林很骄傲', '秦淮两岸风光好，苏州园林很骄傲', 
'/static/images/province/jiangSu.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '浙江省', 72687, '/pages/dashboard/index', 
'江南烟雨罩水乡，西湖景色美无双', '江南烟雨罩水乡，西湖景色美无双', 
'/static/images/province/zheJiang.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '安徽', 71687, '/pages/dashboard/index', 
'黄山云海奇石现，紫云峰下涌温泉', '黄山云海奇石现，紫云峰下涌温泉', 
'/static/images/province/anHui.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '福建省', 70687, '/pages/dashboard/index', 
'鼓浪屿清新浪漫，武夷山茶香自然', '鼓浪屿清新浪漫，武夷山茶香自然', 
'/static/images/province/fuJian.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '江西省', 69687, '/pages/dashboard/index', 
'滕王阁巍峨挺拔，井冈山如诗如画', '滕王阁巍峨挺拔，井冈山如诗如画', 
'/static/images/province/jiangXi.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '山东省', 68687, '/pages/dashboard/index', 
'泰山蓬莱趵突泉，还有鲁菜和海鲜', '泰山蓬莱趵突泉，还有鲁菜和海鲜', 
'/static/images/province/shanDong.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '河南省', 67687, '/pages/dashboard/index', 
'韩愈刘禹锡潘安，少林白马和嵩山', '韩愈刘禹锡潘安，少林白马和嵩山', 
'/static/images/province/heNan.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '湖北省', 66687, '/pages/dashboard/index', 
'长江大桥真不少，热干面非常劲道', '长江大桥真不少，热干面非常劲道', 
'/static/images/province/huBei.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '湖南省', 65687, '/pages/dashboard/index', 
'凤凰古城烟雨飘，臭豆腐是真的好', '凤凰古城烟雨飘，臭豆腐是真的好', 
'/static/images/province/huNan.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '广东省', 64687, '/pages/dashboard/index', 
'粤语听着挺时髦，夜景就看小蛮腰', '粤语听着挺时髦，夜景就看小蛮腰', 
'/static/images/province/guangDong.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '海南省', 63687, '/pages/dashboard/index', 
'天涯海角不用找，日月湾冲浪最妙', '天涯海角不用找，日月湾冲浪最妙', 
'/static/images/province/haiNan.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '四川省', 62687, '/pages/dashboard/index', 
'山川河流全都有，不能错过九寨沟', '山川河流全都有，不能错过九寨沟', 
'/static/images/province/siChuan.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '贵州省', 61687, '/pages/dashboard/index', 
'黄果树瀑布真好，千户苗寨风景妙', '黄果树瀑布真好，千户苗寨风景妙', 
'/static/images/province/guiZhou.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '云南省', 60687, '/pages/dashboard/index', 
'一年四季百花开，珍禽异兽数不来', '一年四季百花开，珍禽异兽数不来', 
'/static/images/province/yunNan.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '陕西省', 59687, '/pages/dashboard/index', 
'多朝古都不是吹，保护文物一大堆', '多朝古都不是吹，保护文物一大堆', 
'/static/images/province/shanXiDaYanTa.jpeg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '甘肃省', 58687, '/pages/dashboard/index', 
'嘉峪关雄伟壮观，莫高窟意义深远', '嘉峪关雄伟壮观，莫高窟意义深远', 
'/static/images/province/ganSu.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '青海省', 57687, '/pages/dashboard/index', 
'青海湖风景适宜，可可西里真神秘', '青海湖风景适宜，可可西里真神秘', 
'/static/images/province/qingHai.jpg', '2025-02-18 09:09:09', null, null, null);

INSERT INTO relaystatistics (Id, Title, ProvinceName, Heat, Url, Label1, Label2, Image, CreatedBy, UpdatedBy, CreatedTime, UpdatedTime)
VALUES (uuid(), '热度', '中国台湾', 56687, '/pages/dashboard/index', 
'祖国母亲的宝岛，领土一寸都不少', '祖国母亲的宝岛，领土一寸都不少', 
'/static/images/province/taiWan.jpg', '2025-02-18 09:09:09', null, null, null);