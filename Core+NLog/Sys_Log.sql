USE [LastChance]
GO

/****** Object:  Table [dbo].[Sys_Log]    Script Date: 2018/12/16 22:48:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Sys_Log](
	[Id] [varchar](50) NOT NULL,
	[Application] [varchar](200) NULL,
	[Logger] [varchar](200) NULL,
	[Level] [varchar](50) NULL,
	[Message] [varchar](500) NULL,
	[Exception] [varchar](500) NULL,
	[Callsite] [varchar](200) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Sys_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Sys_Log] ADD  CONSTRAINT [DF_Sys_Log_Id]  DEFAULT (newid()) FOR [Id]
GO

