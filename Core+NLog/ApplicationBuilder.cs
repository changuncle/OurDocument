﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ConsoleTest.Pipeline
{
    public class ApplicationBuilder
    {
        private static readonly IList<Func<RequestDelegate, RequestDelegate>> _components = new List<Func<RequestDelegate, RequestDelegate>>();

        public ApplicationBuilder Use(Func<HttpContext,Func<Task>,Task> extMiddleware)
        {
            Func<RequestDelegate, RequestDelegate> middleware = next =>
            {
                RequestDelegate result = context =>
                {
                    Func<Task> func = () => next(context);
                    return extMiddleware(context, func);
                };
                return result;
            };
            return Use(middleware);
        }

        public ApplicationBuilder Use(Func<RequestDelegate,RequestDelegate> middleware)
        {
            _components.Add(middleware);
            return this;
        }

        public RequestDelegate Build()
        {
            //app变量就是单个委托
            RequestDelegate app = context => 
            {
                Console.WriteLine("Default Middleware");
                return Task.CompletedTask;
            };

            foreach (Func<RequestDelegate,RequestDelegate> middleware in _components.Reverse())
            {
                app = middleware(app);
            }
            return app;
        } 
    }
}
