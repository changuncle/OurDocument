﻿using System;
using System.Threading.Tasks;

namespace ConsoleTest.Pipeline
{
    public delegate Task RequestDelegate(HttpContext context);

    class Program
    {
        static void Main(string[] args)
        {
            #region 封装执行
            //var app = new ApplicationBuilder();

            //app.Use(async (context, next) =>
            //{
            //    Console.WriteLine("Middleware 1 Begin");
            //    await next();
            //    Console.WriteLine("Middleware 1 End");
            //});

            //app.Use(async (context, next) =>
            //{
            //    Console.WriteLine("Middleware 2 Begin");
            //    await next();
            //    Console.WriteLine("Middleware 2 End");
            //});

            //RequestDelegate firstMiddleware = app.Build();
            //firstMiddleware(new HttpContext()); 
            #endregion

            #region 理解Func<RequestDelegate,RequestDelegate>
            RequestDelegate app = context =>
            {
                Console.WriteLine("Default Middleware");
                return Task.CompletedTask;
            };
            Func<HttpContext, Func<Task>, Task> extMiddleware1 = async(context, next) => 
            {
                Console.WriteLine("Middleware 1 Begin");
                await next();
                Console.WriteLine("Middleware 1 End");
            };
            Func<RequestDelegate, RequestDelegate> middleware1 = next =>
            {
                RequestDelegate result = context =>
                {
                    Func<Task> func = () => next(context);
                    return extMiddleware1(context, func);
                };
                return result;
            };

            Func<HttpContext, Func<Task>, Task> extMiddleware2 = async (context, next) =>
            {
                Console.WriteLine("Middleware 2 Begin");
                await next();
                Console.WriteLine("Middleware 2 End");
            };
            Func<RequestDelegate, RequestDelegate> middleware2 = next =>
            {
                RequestDelegate result = context =>
                {
                    Func<Task> func = () => next(context);
                    return extMiddleware2(context, func);
                };
                return result;
            };

            //Func<RequestDelegate, RequestDelegate>第一次执行，传入RequestDelegate参数app创建并返回委托d1
            //d1的功能：将参数app封装成Func<Task>、以Func<Task>为extMiddleware1中形参next的参数值调用extMiddleware1
            app = middleware1(app);
            //Func<RequestDelegate, RequestDelegate>第一次执行，传入RequestDelegate参数d1创建并返回委托d2
            //d2的功能：将参数d1封装成Func<Task>、以Func<Task>为extMiddleware2中形参next的参数值调用extMiddleware2
            //此时后配置组件中await next()之前的内容会先执行，照常理我们习惯配置在前的组件先执行因此反转一下顺序即可满足要求
            app = middleware2(app);
            //Func<RequestDelegate, RequestDelegate>第二次执行，传入HttpContext执行委托d2
            app(new HttpContext());
            #endregion

            Console.ReadKey();
        }
    }
}
