﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using H3C.TS.Web.Models;
using System.Text;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NLog;

namespace H3C.TS.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            this._logger = logger;
        }

        public IActionResult Index()
        {

            try
            {
                #region 调用类库存取Session
                //string key = "UserInfo";
                //BasicUserInfo userInfo = new BasicUserInfo()
                //{
                //    UserID = Guid.NewGuid().ToString(),
                //    UserCode = "99999",
                //    UserName = "guo"
                //};
                //ServiceContext serviceContext = new ServiceContext(HttpContext);
                //serviceContext.SetSessionData<BasicUserInfo>(key, userInfo);
                //BasicUserInfo result = serviceContext.GetSessionData<BasicUserInfo>(key + "aaa");
                //serviceContext.SetBasicUserInfo(userInfo);
                //BasicUserInfo basicUserInfo = serviceContext.GetBasicUserInfo();
                #endregion

                #region Controller中存取Session
                //string key = "UserInfo";
                //BasicUserInfo userInfo = new BasicUserInfo()
                //{
                //    UserID = Guid.NewGuid().ToString(),
                //    UserCode = "99999",
                //    UserName = "guo"
                //};
                //byte[] bytes = Encoding.Default.GetBytes(JsonConvert.SerializeObject(userInfo));
                //HttpContext.Session.Set(key, bytes);
                //bytes = HttpContext.Session.Get(key);
                //BasicUserInfo result = JsonConvert.DeserializeObject<BasicUserInfo>(Encoding.Default.GetString(bytes));
                #endregion

                #region 测试ConfigUtility
                //string data = AppsettingsUtility.Data;
                //string allowedHosts = AppsettingsUtility.AllowedHosts;
                //Logging logging = AppsettingsUtility.Logging;
                //UserInfo userInfo = AppsettingsUtility.UserInfo;
                //string sSO_SSOWebService_SSOService = AppsettingsUtility.SSO_SSOWebService_SSOService;
                //string connectionKey = AppsettingsUtility.ConnectionKey;
                #endregion

                #region 依赖注入
                //_logger.LogInformation("Index page says Hello");
                //_logger.LogError("Error");
                //string str1 = null;
                //if (str1.Equals("guo"))
                //{
                //    str1 = "newGuo";
                //}
                #endregion

                #region 创建实例
                //Logger _cLogger = LogManager.GetCurrentClassLogger();
                //_cLogger.Info("Index page says Hello");
                //_cLogger.Error("Error");
                //string str1 = null;
                //if (str1.Equals("guo"))
                //{
                //    str1 = "newGuo";
                //}
                #endregion
                LogHelper.Info("帮助类Info");
                LogHelper.Warn("帮助类Warn");
                LogHelper.Error("帮助类Error");
                string str1 = null;
                if (str1.Equals("guo"))
                {
                    str1 = "newGuo";
                }
            }
            catch (Exception ex)
            {
                #region 创建实例
                //Logger _cLogger = LogManager.GetCurrentClassLogger();
                //_cLogger.Error(ex, "Error");
                #endregion
                LogHelper.ErrorException("帮助类ErrorException", ex);
            }
                return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
