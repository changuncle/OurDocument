/***MySql_InnoDB引擎简介***
 1、MySql默认存储引擎InnoDB实现了基于多版本的控制并发控制协议——MVCC(Multiple Version Concurrency Control)
 2、与MVCC相对的是基于锁定并发控制——LBCC（Lock Based Concurrency Control）
 3、MVCC最大的好处是除serializable外读不加锁，读写不冲突，在读多写少的OLTP应用中，读写不冲突非常重要，能极大的提高的系统的并发性能
   MVCC就是将一份数据临时保存多个版本，进而实现并发控制
 4、MVCC并发控制将读操作分为两类：快照读和当前读
   快照读（简单的select操作）：读取的是记录中的可见版本（可能是历史版本），不会加锁
   当前读（特殊的select操作、insert、delete、update）：读取的是记录中的最新版本，而且为返回的记录加上锁，避免其他事务并发修改这些记录
 5、间隙锁是InnoDB仅在可重复读隔离级别下为了解决幻读问题而引入的锁机制
 6、间隙锁的检索条件和主键之间有什么关系？
*/

#查看数据库引擎
show engine InnoDB status;

#查看数据库隔离级别
select @@tx_isolation;

#设置数据库隔离级别
set session transaction isolation level read uncommitted;
set session transaction isolation level read committed;
set session transaction isolation level repeatable read;
set session transaction isolation level serializable;

#查看是否自动提交
select @@autocommit;

#设置是否自动提交
set @@autocommit=0;

#查看数据库锁
select * from information_schema.INNODB_LOCKS;
select * from information_schema.INNODB_LOCK_WAITS;

#查看当前运行的所有事务
select * from information_schema.innodb_trx;

#手动添加共享锁
select * from sign_user WHERE Id='05e7cc5f-391c-4cf2-97c6-c023e8804ded' lock in SHARE MODE;

#手动添加排他锁
select * from sign_user WHERE Id='05e7cc5f-391c-4cf2-97c6-c023e8804ded' for update;

#ROW_NUMBER()效果
set @row= 2;
select @row:=@row+1 as rowIndex,sign_user.* from sign_user ORDER BY Charger;
set @row= 2;
select @row:=case when @row is null then 1 else @row+1 end as rowIndex,sign_user.* from sign_user ORDER BY Charger;
select @row:=@row+1 as rowIndex,sign_user.* from sign_user,(SELECT @row:=2) AS foo ORDER BY Charger;

#DENSE_RANK()效果
select sign_user.Id,sign_user.UserName,sign_user.UserPassword,sign_user.Charger
  ,CASE
	WHEN @preValue=sign_user.Charger THEN @curRank
	ELSE @curRank:=@curRank+1
  END as rowIndex
  ,@preValue:=sign_user.Charger as preValue
from sign_user,(select @curRank:=0,@preValue:=null)foo
order by Charger;

#RANK()效果
select sign_user.Id,sign_user.UserName,sign_user.UserPassword,sign_user.Charger
  ,@curRank:=IF(@preValue=sign_user.Charger,@curRank,@incRank) as rowIndex
  ,@incRank:=@incRank+1 as nextRowIndex
  ,@preValue:=sign_user.Charger as preValue
from sign_user,(select @curRank:=0,@incRank:=1,@preValue:=null)foo
order by Charger;
