﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace TestAD.Web
{
    #region 用户属性定义标志

    /// <summary>
    /// 用户属性定义标志
    /// </summary>
    public enum ADS_USER_FLAG_ENUM
    {
        /// <summary>
        /// 登录脚本标志。如果通过 ADSI LDAP 进行读或写操作时，该标志失效。如果通过 ADSI WINNT，该标志为只读。
        /// </summary>
        ADS_UF_SCRIPT = 0X0001,

        /// <summary>
        /// 用户帐号禁用标志        
        /// </summary>
        ADS_UF_ACCOUNTDISABLE = 0X0002,

        /// <summary>
        /// 主文件夹标志
        /// </summary>
        ADS_UF_HOMEDIR_REQUIRED = 0X0008,

        /// <summary>
        /// 过期标志
        /// </summary>
        ADS_UF_LOCKOUT = 0X0010,

        /// <summary>
        /// 用户密码不是必须的
        /// </summary>
        ADS_UF_PASSWD_NOTREQD = 0X0020,

        /// <summary>
        /// 密码不能更改标志
        /// </summary>
        ADS_UF_PASSWD_CANT_CHANGE = 0X0040,

        /// <summary>
        /// 使用可逆的加密保存密码
        /// </summary>
        ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = 0X0080,

        /// <summary>
        /// 本地帐号标志
        /// </summary>
        ADS_UF_TEMP_DUPLICATE_ACCOUNT = 0X0100,

        /// <summary>
        /// 普通用户的默认帐号类型
        /// </summary>
        ADS_UF_NORMAL_ACCOUNT = 0X0200,

        /// <summary>
        /// 跨域的信任帐号标志
        /// </summary>
        ADS_UF_INTERDOMAIN_TRUST_ACCOUNT = 0X0800,

        /// <summary>
        /// 工作站信任帐号标志
        /// </summary>
        ADS_UF_WORKSTATION_TRUST_ACCOUNT = 0x1000,

        /// <summary>
        /// 服务器信任帐号标志
        /// </summary>
        ADS_UF_SERVER_TRUST_ACCOUNT = 0X2000,

        /// <summary>
        /// 密码永不过期标志
        /// </summary>
        ADS_UF_DONT_EXPIRE_PASSWD = 0X10000,

        /// <summary>
        /// MNS 帐号标志
        /// </summary>
        ADS_UF_MNS_LOGON_ACCOUNT = 0X20000,

        /// <summary>
        /// 交互式登录必须使用智能卡
        /// </summary>
        ADS_UF_SMARTCARD_REQUIRED = 0X40000,

        /// <summary>
        /// 当设置该标志时，服务帐号（用户或计算机帐号）将通过 Kerberos 委托信任
        /// </summary>
        ADS_UF_TRUSTED_FOR_DELEGATION = 0X80000,

        /// <summary>
        /// 当设置该标志时，即使服务帐号是通过 Kerberos 委托信任的，敏感帐号不能被委托
        /// </summary>
        ADS_UF_NOT_DELEGATED = 0X100000,

        /// <summary>
        /// 此帐号需要 DES 加密类型
        /// </summary>
        ADS_UF_USE_DES_KEY_ONLY = 0X200000,

        /// <summary>
        /// 不要进行 Kerberos 预身份验证
        /// </summary>
        ADS_UF_DONT_REQUIRE_PREAUTH = 0X4000000,

        /// <summary>
        /// 用户密码过期标志
        /// </summary>
        ADS_UF_PASSWORD_EXPIRED = 0X800000,

        /// <summary>
        /// 用户帐号可委托标志
        /// </summary>
        ADS_UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION = 0X1000000

    }

    #endregion

    #region 用户组类型
    [Flags]
    public enum ADS_GROUP_TYPE
    {
        /// <summary>
        /// 全局
        /// </summary>
        ADS_GROUP_TYPE_GLOBAL_GROUP = 0x00000002,
        /// <summary>
        /// 本地域
        /// </summary>
        ADS_GROUP_TYPE_LOCAL_GROUP = 0x00000004,
        /// <summary>
        /// 通用
        /// </summary>
        ADS_GROUP_TYPE_UNIVERSAL_GROUP = 0x00000008,
        /// <summary>
        /// 安全组
        /// </summary>
        ADS_GROUP_TYPE_SECURITY_ENABLED = unchecked((int)0x80000000)

        //默认是通讯组
    }
    #endregion

    public class ADHelper
    {
        public ADHelper()
        {

        }

        /// <summary>
        /// 获得DirectoryEntry对象实例,以管理员登陆AD
        /// </summary>
        /// <returns></returns>
        private static DirectoryEntry GetDirectoryObject()
        {
            DirectoryEntry entry = null;
            try
            {
                //【写法1】
                //entry = new DirectoryEntry("LDAP://192.168.0.188", @"test\wangfei", "Hello123", AuthenticationTypes.Secure);
                //【写法2】
                entry = new DirectoryEntry(ConfigHelper.AdServer, ConfigHelper.AdUser, ConfigHelper.AdPassword, AuthenticationTypes.Secure);
            }
            catch (Exception ex)
            {
            }
            entry.RefreshCache();
            return entry;
        }

        #region 获取用户
        /// <summary>
        /// 根据通用名获取用户的对象
        /// </summary>
        /// <param name="commonName">通用名(cn)</param>
        /// <returns>如果找到则返回用户的对象,否则返回 null</returns>
        public static DirectoryEntry GetUserByCn(string commonName)
        {
            return GetUser("cn", commonName);
        }

        /// <summary>
        /// 根据sam名称获取用户的对象
        /// </summary>
        /// <param name="samName">sam名称(sAMAccountName)</param>
        /// <returns>如果找到则返回用户的对象,否则返回 null</returns>
        public static DirectoryEntry GetUserBySam(string samName)
        {
            return GetUser("sAMAccountName", samName);
        }

        /// <summary>
        /// 根据用户名获取用户的对象
        /// </summary>
        /// <param name="nameType">用户名类型(cn/sam)</param>
        /// <param name="name">用户名</param>
        /// <returns>如果找到则返回用户的对象,否则返回 null</returns>
        public static DirectoryEntry GetUser(string nameType, string name)
        {
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(&(objectCategory=person)(objectClass=user))(" + nameType + "=" + name.Replace("\\", "") + "))";
            deSearch.SearchScope = SearchScope.Subtree;
            try
            {
                SearchResult result = deSearch.FindOne();
                de = new DirectoryEntry(result.Path, ConfigHelper.AdUser, ConfigHelper.AdPassword);
                return de;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 获取域上的所有用户
        /// </summary>
        /// <returns>用户集合</returns>               
        public static ArrayList GetUsers()
        {
            ArrayList users = new ArrayList();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(&(objectCategory=person)(objectClass=user)))";
            deSearch.SearchScope = SearchScope.Subtree;
            try
            {
                SearchResultCollection results = deSearch.FindAll();
                for (int i = 0; i < results.Count; i++)
                {
                    DirectoryEntry des = results[i].GetDirectoryEntry();
                    users.Add(des);
                }
                return users;
            }
            catch (Exception ex)
            {
                return null;
            }
        } 
        #endregion

        #region 获取群组
        /// <summary>
        /// 根据通用名获取用户组的对象
        /// </summary>
        /// <param name="commonName">通用名(cn)</param>
        /// <returns>用户组</returns>
        public static DirectoryEntry GetGroupByCn(string commonName)
        {
            return GetGroup("cn", commonName);
        }

        /// <summary>
        /// 根据sam名称获取用户组的对象
        /// </summary>
        /// <param name="commonName">sam名称(sAMAccountName)</param>
        /// <returns>用户组</returns>
        public static DirectoryEntry GetGroupBySam(string samName)
        {
            return GetGroup("sAMAccountName", samName);
        }

        /// <summary>
        /// 根据通用名获取用户组的对象
        /// </summary>
        /// <param name="nameType">用户名类型(cn/sam)</param>
        /// <param name="name">用户名</param>
        /// <returns>用户组</returns>
        public static DirectoryEntry GetGroup(string nameType, string name)
        {
            ArrayList groups = new ArrayList();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(&(objectCategory=group))(" + nameType + "=" + name.Replace("\\", "") + "))";
            deSearch.SearchScope = SearchScope.Subtree;
            try
            {
                SearchResult result = deSearch.FindOne();
                //[1]Web环境可删除，Windows服务环境不可删除
                //de = new DirectoryEntry(result.Path);
                //[2]Web环境、Windows服务环境均可删除
                de = result.GetDirectoryEntry();
                return de;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 获取域上的所有用户组
        /// </summary>
        /// <returns>用户集合</returns>
        public static ArrayList GetGroups()
        {
            ArrayList groups = new ArrayList();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(objectCategory=group)";
            deSearch.SearchScope = SearchScope.Subtree;
            SearchResultCollection results;
            try
            {
                results = deSearch.FindAll();
                foreach (SearchResult sr in results)
                {
                    DirectoryEntry directoryentry = sr.GetDirectoryEntry();
                    groups.Add(directoryentry);
                }
                return groups;
            }
            catch (Exception ex)
            {
                return null;
            }
        } 
        
        /// <summary> 
        /// 根据用户组名称获取域上用户组（支持模糊查询）
        /// </summary> 
        /// <returns>用户集合</returns> 
        public static List<string> GetGroups(string groupName)
        {
            List<string> groups = new List<string>();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(objectClass=group)(cn=*" + groupName + "*))";
            deSearch.SearchScope = SearchScope.Subtree;
            SearchResultCollection results;
            try
            {
                results = deSearch.FindAll();
                foreach (SearchResult sr in results)
                {
                    DirectoryEntry directoryentry = sr.GetDirectoryEntry();
                    string name = directoryentry.Name;
                    string[] GroupName = name.Split('=');
                    if (GroupName.Length > 1 && GroupName[1].StartsWith("&"))
                    {
                        groups.Add(GroupName[1]);
                    }
                }
                return groups;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        /// <summary>
        /// 根据通用名获取组织单位的对象
        /// </summary>
        /// <param name="commonName">通用名(cn)</param>
        /// <returns>组织单位</returns>
        public static DirectoryEntry GetOu(string commonName)
        {
            ArrayList groups = new ArrayList();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(&(objectClass=organizationalUnit))(ou=" + commonName.Replace("\\", "") + "))";
            deSearch.SearchScope = SearchScope.Subtree;
            try
            {
                SearchResult result = deSearch.FindOne();
                de = new DirectoryEntry(result.Path);
                return de;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 获取域上的所有组织单位
        /// </summary>
        /// <returns>组织单位集合</returns>
        public static ArrayList GetOus()
        {
            ArrayList groups = new ArrayList();
            DirectoryEntry de = GetDirectoryObject();
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(objectClass=organizationalUnit)";
            deSearch.SearchScope = SearchScope.Subtree;
            SearchResultCollection results;
            try
            {
                results = deSearch.FindAll();
                foreach (SearchResult sr in results)
                {
                    DirectoryEntry directoryentry = sr.GetDirectoryEntry();
                    groups.Add(directoryentry);
                }
                return groups;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region 查找某组中的成员
        /// <summary>
        /// 查找某用户组中的成员
        /// </summary>
        /// <param name="groupName">用户组名称</param>
        /// <returns>用户集合</returns>
        public static ArrayList GetMembersByGroup(string groupName)
        {
            ArrayList users = new ArrayList();
            DirectoryEntry SearchRoot = GetDirectoryObject();
            DirectorySearcher directorySearch = new DirectorySearcher(SearchRoot);
            directorySearch.Filter = "(&(objectClass=group)(SAMAccountName=" + groupName + "))";
            SearchResult result = directorySearch.FindOne();
            try
            {
                if (result != null)
                {
                    DirectoryEntry dirEntry = new DirectoryEntry(result.Path, ConfigHelper.AdUser, ConfigHelper.AdPassword);
                    System.DirectoryServices.PropertyCollection propertyCollection = dirEntry.Properties;
                    int count = propertyCollection["member"].Count;
                    for (int i = 0; i < count; i++)
                    {
                        string respath = result.Path;
                        string[] pathnavigate = respath.Split("CN".ToCharArray());
                        respath = pathnavigate[0];
                        string objpath = propertyCollection["member"][i].ToString();
                        string path = respath + objpath;
                        DirectoryEntry user = new DirectoryEntry(path, ConfigHelper.AdUser, ConfigHelper.AdPassword);
                        users.Add(user);
                    }
                }
                return users;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region 根据用户名查找对应的组
        /// </summary>
        /// 根据用户名查找对应的组
        /// </summary>
        /// <param name="username">用户名</param>
        /// <returns>ArrayList用户组集合</returns>
        public static ArrayList GetGroupsByUser(string username)
        {
            DirectoryEntry directoryEntry = GetDirectoryObject();
            DirectorySearcher ds = new DirectorySearcher(directoryEntry);
            ds.Filter = "(&(sAMAccountName=" + username + "))";
            ds.PropertiesToLoad.Add("memberof");
            SearchResult searchresult = ds.FindOne();
            ArrayList groupNames = new ArrayList();
            if (searchresult == null)
            {
                groupNames = null;
            }
            else
            {
                if (searchresult.Properties["memberof"] == null)
                {
                    return (null);
                }
                for (int i = 0; i < searchresult.Properties["memberof"].Count; i++)
                {
                    string theGroupPath = searchresult.Properties["memberof"][i].ToString();
                    groupNames.Add(theGroupPath.Substring(3, theGroupPath.IndexOf(",") - 3));
                }
            }
            directoryEntry.Close();
            return groupNames;
        }
        #endregion

        #region 添加用户
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="ldapDN">路径</param>
        /// <param name="userModel">用户实体对象</param>
        public static void AddUser(string ldapDN, DomainUser userModel)
        {
            string ou = "";
            AddUser(ldapDN, ou, userModel);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="ldapDN">路径</param>
        /// <param name="ou">OU目录</param>
        /// <param name="userModel">用户实体对象</param>
        public static void AddUser(string ldapDN, string ou, DomainUser userModel)
        {
            try
            {
                #region 若域中已存在同名账户则不再创建
                if (GetUserByCn(userModel.UserName) != null)
                {
                    return;
                }
                #endregion

                if (!string.IsNullOrEmpty(ou) && !ou.StartsWith(","))
                {
                    ou = "," + ou;
                }
                DirectoryEntry entry = GetDirectoryObject();
                DirectoryEntry subEntry = new DirectoryEntry(ldapDN, ConfigHelper.AdUser, ConfigHelper.AdPassword);
                DirectoryEntry deUser = subEntry.Children.Add("CN=" + userModel.UserName + ou, "user");
                deUser.Properties["sAMAccountName"].Value = userModel.SAMAccountName;
                if (userModel.Description != "")
                {
                    deUser.Properties["description"].Value = userModel.Description;
                }
                if (userModel.Email != "")
                {
                    deUser.Properties["Mail"].Value = userModel.Email;
                }
                if (userModel.Tel != "")
                {
                    deUser.Properties["telephoneNumber"].Add(userModel.Tel);
                }
                deUser.CommitChanges();
                SetPassword(userModel.UserName, userModel.Password);
                EnableUser(userModel.UserName);
                deUser.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region 删除用户
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="commonName">用户名</param>
        public static void DeleteUser(string commonName)
        {
            DirectoryEntry de = GetUserByCn(commonName);
            try
            {
                if (de != null)
                {
                    de.DeleteTree();
                    de.CommitChanges();
                    de.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("【删除用户失败】commonName=" + commonName + "，ex.Message=" + ex.Message + "，ex.ToString=" + ex.ToString());
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="commonName">用户名</param>
        public static void DeleteUserBySam(string samName)
        {
            DirectoryEntry de = GetUserBySam(samName);
            try
            {
                if (de != null)
                {
                    de.DeleteTree();
                    de.CommitChanges();
                    de.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("【删除用户失败】samName=" + samName + "，ex.Message=" + ex.Message + "，ex.ToString=" + ex.ToString());
            }
        }
        #endregion

        #region 添加用户组
        /// <summary>
        /// 添加用户组
        /// </summary>
        /// <param name="ldap">路径</param>
        /// <param name="domainGroup">用户组实体对象</param>
        public static void AddGroup(string ldap, DomainGroup domainGroup)
        {
            string ou = "";
            AddGroup(ldap, ou, domainGroup);
        }

        /// <summary>
        /// 添加用户组
        /// </summary>
        /// <param name="ldap">路径</param>
        /// <param name="ou">OU目录</param>
        /// <param name="domainGroup">用户组实体对象</param>
        public static void AddGroup(string ldap, string ou, DomainGroup domainGroup)
        {
            try
            {
                #region 若域中已存在同名组则不再创建
                if (GetGroupByCn(domainGroup.GroupName) != null)
                {
                    return;
                }
                #endregion

                if (!string.IsNullOrEmpty(ou) && !ou.StartsWith(","))
                {
                    ou = "," + ou;
                }
                using (DirectoryEntry dir = new DirectoryEntry(ldap, ConfigHelper.AdUser, ConfigHelper.AdPassword, AuthenticationTypes.Secure))
                {
                    using (DirectoryEntry group = dir.Children.Add("CN=" + domainGroup.GroupName + ou, "group"))
                    {
                        #region 写法1
                        //group.Invoke("Put", new object[] { "Description", domainGroup.Description });
                        //group.Invoke("Put", new object[] { "CN", domainGroup.GroupName });
                        //group.Invoke("Put", new object[] { "sAMAccountName", domainGroup.SAMAccountName });
                        //group.Invoke("Put", new object[] { "groupType", domainGroup.GroupType }); 
                        #endregion

                        #region 写法2
                        group.Properties["Description"].Value = domainGroup.Description;
                        group.Properties["CN"].Value = domainGroup.GroupName;
                        group.Properties["sAMAccountName"].Value = domainGroup.SAMAccountName;
                        group.Properties["groupType"].Value = domainGroup.GroupType;
                        #endregion
                        group.CommitChanges();
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion

        #region 修改用户组
        /// <summary>
        /// 修改组名
        /// </summary>
        /// <param name="oldGroupName">原始组名</param>
        /// <param name="newGroupName">新组名</param>
        /// <returns></returns>
        public static bool UpdateGroupName(string oldGroupName, string newGroupName)
        {
            try
            {
                DirectoryEntry group = GetGroupByCn(oldGroupName);
                if (null != group)
                {
                    if (!string.IsNullOrEmpty(newGroupName))
                    {
                        group.Rename("cn=" + newGroupName);
                        group.CommitChanges();
                        group.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 修改群组描述信息
        /// </summary>
        /// <param name="groupName">组名</param>
        /// <param name="desc">描述信息</param>
        /// <returns></returns>
        public static bool UpdateGroupDesc(string groupName, string desc)
        {
            try
            {
                DirectoryEntry group = GetGroupByCn(groupName);
                if (group != null)
                {
                    group.Properties["description"].Value = desc;
                    group.Properties["info"].Value = desc;
                    group.CommitChanges();
                    group.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region 删除用户组
        /// <summary>
        /// 删除用户组
        /// </summary>
        /// <param name="commonName">用户组名</param>
        public static void DeleteGroup(string commonName)
        {
            DirectoryEntry de = GetGroupByCn(commonName);
            try
            {
                if (de != null)
                {
                    de.DeleteTree();
                    de.CommitChanges();
                    de.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("【删除用户组失败】commonName=" + commonName + "，ex.Message=" + ex.Message + "，ex.ToString=" + ex.ToString());
            }
        }
        #endregion

        #region 添加组织单位
        /// <summary>
        /// 添加组织单位
        /// </summary>
        /// <param name="ldap">路径</param>
        /// <param name="domainOu">组织单位实体</param>
        public static void AddOu(string ldap, DomainOu domainOu)
        {
            string ou = "";
            AddOu(ldap, ou, domainOu);
        }

        /// <summary>
        /// 添加组织单位
        /// </summary>
        /// <param name="ldap">路径</param>
        /// <param name="ou">OU目录</param>
        /// <param name="domainOu">组织单位实体</param>
        public static void AddOu(string ldap, string ou, DomainOu domainOu)
        {
            try
            {
                #region 若域中已存在同名组织单位则不再创建
                if (GetOu(domainOu.OuName) != null)
                {
                    return;
                }
                #endregion

                if (!string.IsNullOrEmpty(ou) && !ou.StartsWith(","))
                {
                    ou = "," + ou;
                }
                using (DirectoryEntry dir = new DirectoryEntry(ldap, ConfigHelper.AdUser, ConfigHelper.AdPassword, AuthenticationTypes.Secure))
                {
                    using (DirectoryEntry group = dir.Children.Add("OU=" + domainOu.OuName + ou, "organizationalUnit"))
                    {
                        if (domainOu.Description != "")
                        {
                            group.Invoke("Put", new object[] { "Description", domainOu.Description });
                        }
                        group.CommitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region 删除组织单位
        /// <summary>
        /// 删除组织单位
        /// </summary>
        /// <param name="commonName">组织单元名</param>        
        public static void DeleteOu(string commonName)
        {
            DirectoryEntry de = GetOu(commonName);
            try
            {
                if (de != null)
                {
                    de.DeleteTree();
                    de.CommitChanges();
                    de.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region 设置域账号的密码
        /// <summary>
        /// 设置域账号的密码
        /// </summary>
        /// <param name="commonName">通用名（cn）</param>
        /// <param name="password">密码</param>
        public static string SetPassword(string commonName, string password)
        {
            //获取对应AD实体
            DirectoryEntry user = GetUserByCn(commonName);
            try
            {
                user.Invoke("SetPassword", new object[] { password });
                user.CommitChanges();
                user.Close();
            }
            catch (Exception ex)
            {
                return "";
            }
            return user.Path;
        }
        #endregion

        #region 修改域账号的密码
        /// <summary>
        /// 修改域账号的密码
        /// </summary>
        /// <param name="commonName">用户公共名称</param>
        /// <param name="oldPassword">旧密码</param>
        /// <param name="newPassword">新密码</param>
        public static string ChangePassword(string commonName, string oldPassword, string newPassword)
        {
            // to-do: 需要解决密码策略问题
            DirectoryEntry user = GetUserByCn(commonName);
            try
            {
                user.Invoke("ChangePassword", new Object[] { oldPassword, newPassword });
                user.CommitChanges();
                user.Close();
            }
            catch (Exception ex)
            {
                return "";
            }
            return user.Path;
        }
        #endregion

        #region 禁用指定名称的用户
        /// <summary>
        /// 禁用指定名称的用户
        /// </summary>
        /// <param name="commonName">用户公共名称</param>
        public static void DisableUser(string commonName)
        {
            DisableUser(GetUserByCn(commonName));
        }
        #endregion

        #region 禁用指定的用户
        /// <summary>
        ///禁用指定的用户
        /// </summary>
        /// <param name="de"></param>
        public static void DisableUser(DirectoryEntry de)
        {
            de.Properties["userAccountControl"][0] =
                ADS_USER_FLAG_ENUM.ADS_UF_NORMAL_ACCOUNT |
                ADS_USER_FLAG_ENUM.ADS_UF_DONT_EXPIRE_PASSWD |
                ADS_USER_FLAG_ENUM.ADS_UF_ACCOUNTDISABLE;
            de.CommitChanges();
            de.Close();
        }
        #endregion

        #region 启用指定名称的用户
        /// <summary>
        /// 启用指定名称的用户
        /// </summary>
        /// <param name="commonName"></param>
        public static void EnableUser(string commonName)
        {
            EnableUser(GetUserByCn(commonName));
        }

        #endregion

        #region 启用指定的用户
        /// <summary>
        /// 启用指定的用户
        /// </summary>
        /// <param name="de"></param>
        public static void EnableUser(DirectoryEntry de)
        {
            de.Properties["userAccountControl"][0] =
                ADS_USER_FLAG_ENUM.ADS_UF_NORMAL_ACCOUNT |
                ADS_USER_FLAG_ENUM.ADS_UF_DONT_EXPIRE_PASSWD;
            de.CommitChanges();
            de.Close();
        }
        #endregion

        #region 将用户添加到用户组中
        /// <summary>
        /// 将用户添加到用户组中
        /// </summary>
        /// <param name="userCommonName">用户名(cn)</param>
        /// <param name="groupCommonName">用户组名(cn)</param>
        /// <returns></returns>
        public static bool AddCnuserToGroup(string userCommonName, string groupCommonName)
        {
            try
            {
                DirectoryEntry oGroup = GetGroupByCn(groupCommonName);
                DirectoryEntry oUser = GetUserByCn(userCommonName);
                if (oGroup != null && oUser != null)
                {
                    string path = oUser.Path.Substring(oUser.Path.IndexOf("CN="));
                    oGroup.Properties["Member"].Add(path);
                    oGroup.CommitChanges();
                    oGroup.Close();
                    oUser.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("【将用户添加到用户组中】userCommonName=" + userCommonName + "，groupCommonName=" + groupCommonName + "，ex.Message=" + ex.Message + "，ex.ToString=" + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 将用户添加到用户组中
        /// </summary>
        /// <param name="userSamName">用户名(cn)</param>
        /// <param name="groupCommonName">用户组名(cn)</param>
        /// <returns></returns>
        public static bool AddSamuserToGroup(string userSamName, string groupCommonName)
        {
            try
            {
                DirectoryEntry oGroup = GetGroupByCn(groupCommonName);
                DirectoryEntry oUser = GetUserBySam(userSamName);
                if (oGroup != null && oUser != null)
                {
                    string path = oUser.Path.Substring(oUser.Path.IndexOf("CN="));
                    oGroup.Properties["Member"].Add(path);
                    oGroup.CommitChanges();
                    oGroup.Close();
                    oUser.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("【将用户添加到用户组中】userSamName=" + userSamName + "，groupCommonName=" + groupCommonName + "，ex.Message=" + ex.Message + "，ex.ToString=" + ex.ToString());
                return false;
            }
        }
        #endregion

        #region 判断用户是否在指定的组
        /// <summary>
        /// 判断用户是否在指定的组
        /// </summary>
        /// <param name="groupEntry">域组名称(cn)</param>
        /// <param name="userName">用户名称(cn)</param>
        /// <returns></returns>
        public static bool IsCnuserInGroup(string groupName, string userName)
        {
            DirectoryEntry groupEntry = GetGroupByCn(groupName);
            DirectoryEntry userEntry = GetUserByCn(userName);
            return IsUserInGroup(groupEntry, userEntry);
        }

        /// <summary>
        /// 判断用户是否在指定的组
        /// </summary>
        /// <param name="groupEntry">域组名称(cn)</param>
        /// <param name="userName">用户名称(cn)</param>
        /// <returns></returns>
        public static bool IsSamuserInGroup(string groupName, string userName)
        {
            DirectoryEntry groupEntry = GetGroupByCn(groupName);
            DirectoryEntry userEntry = GetUserBySam(userName);
            return IsUserInGroup(groupEntry, userEntry);
        }

        /// <summary>
        /// 判断用户是否在指定的组
        /// </summary>
        /// <param name="groupEntry">域组对象</param>
        /// <param name="userEntry">用户对象</param>
        /// <returns></returns>
        public static bool IsUserInGroup(DirectoryEntry groupEntry, DirectoryEntry userEntry)
        {
            if (groupEntry == null || userEntry == null)
            {
                return false;
            }
            try
            {
                return groupEntry.Children.Find(userEntry.Name, userEntry.SchemaClassName) != null;
            }
            catch
            {
                string userDistinguishedName = userEntry.Properties["distinguishedName"].Value + "";
                return groupEntry.Properties["member"].Contains(userDistinguishedName);
            }
        } 
        #endregion

        #region 从指定的组中删除用户
        /// <summary>
        /// 从指定的组中删除用户
        /// </summary>
        /// <param name="userCommonName">用户名称</param>
        /// <param name="groupName">用户组名称</param>
        public static bool DeleteCnuserFromGroup(string userCommonName, string groupName)
        {
            try
            {
                DirectoryEntry oGroup = GetGroupByCn(groupName);
                DirectoryEntry oUser = GetUserByCn(userCommonName);
                if (oGroup != null && oUser != null)
                {
                    oGroup.Properties["member"].Remove(oUser.Properties["distinguishedName"].Value);
                    oGroup.CommitChanges();
                    oGroup.Close();
                    oUser.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 从指定的组中删除用户
        /// </summary>
        /// <param name="userSamName">用户名称</param>
        /// <param name="groupName">用户组名称</param>
        public static bool DeleteSamuserFromGroup(string userSamName, string groupName)
        {
            try
            {
                DirectoryEntry oGroup = GetGroupByCn(groupName);
                DirectoryEntry oUser = GetUserBySam(userSamName);
                if (oGroup != null && oUser != null)
                {
                    oGroup.Properties["member"].Remove(oUser.Properties["distinguishedName"].Value);
                    oGroup.CommitChanges();
                    oGroup.Close();
                    oUser.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region 将某用户组添加到用户组中
        /// <summary>
        /// 将某用户组添加到用户组中
        /// </summary>
        /// <param name="srcGroupName">源用户组名</param>
        /// <param name="destGroupName">目标用户组名</param>
        /// <returns></returns>
        public static bool AddGroupToGroup(string srcGroupName, string destGroupName)
        {
            try
            {
                DirectoryEntry srcGroup = GetGroupByCn(srcGroupName);
                DirectoryEntry destGroup = GetGroupByCn(destGroupName);
                if (srcGroup != null && destGroup != null)
                {
                    string path = srcGroup.Path.Substring(srcGroup.Path.IndexOf("CN="));
                    //object obj = srcGroup.Properties["distinguishedName"].Value;
                    destGroup.Properties["Member"].Add(path);
                    destGroup.CommitChanges();
                    destGroup.Close();
                    srcGroup.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region 判断某用户组是否在指定的组
        /// <summary>
        /// 判断某用户组是否在指定的组
        /// </summary>
        /// <param name="childCn">子组名称(cn)</param>
        /// <param name="fatherCn">父组名称(cn)</param>
        /// <returns></returns>
        public static bool IsGroupInGroup(string childCn, string fatherCn)
        {
            DirectoryEntry childGroup = GetGroupByCn(childCn);
            DirectoryEntry fatherGroup = GetGroupByCn(fatherCn);
            return IsGroupInGroup(childGroup, fatherGroup);
        }

        /// <summary>
        /// 判断某用户组是否在指定的组
        /// </summary>
        /// <param name="childGroup">子组对象</param>
        /// <param name="fatherGroup">父组对象</param>
        /// <returns></returns>
        public static bool IsGroupInGroup(DirectoryEntry childGroup, DirectoryEntry fatherGroup)
        {
            if (childGroup == null || fatherGroup == null)
            {
                return false;
            }
            try
            {
                return fatherGroup.Children.Find(childGroup.Name, childGroup.SchemaClassName) != null;
            }
            catch
            {
                string userDistinguishedName = childGroup.Properties["distinguishedName"].Value + "";
                return fatherGroup.Properties["member"].Contains(userDistinguishedName);
            }
        }
        #endregion

        #region 从指定的用户组中删除某用户组
        /// <summary>
        /// 从指定的用户组中删除某用户组
        /// </summary>
        /// <param name="userCommonName">要删除的用户组名称</param>
        /// <param name="groupName">用户组名称</param>
        public static bool DeleteGroupFromGroup(string deletedGroupName, string groupName)
        {
            try
            {
                DirectoryEntry oGroup = GetGroupByCn(groupName);
                DirectoryEntry dGroup = GetGroupByCn(deletedGroupName);
                if (oGroup != null && dGroup != null)
                {
                    oGroup.Properties["member"].Remove(dGroup.Properties["distinguishedName"].Value);
                    oGroup.CommitChanges();
                    oGroup.Close();
                    dGroup.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// 设置指定的属性值
        /// </summary>
        /// <param name="de"></param>
        /// <param name="propertyName">属性名称</param>
        /// <param name="propertyValue">属性值</param>
        public static void SetProperty(DirectoryEntry de, string propertyName, string propertyValue)
        {
            //if (de.Properties.Contains(propertyName))
            //{
            //    if (String.IsNullOrEmpty(propertyValue))
            //    {
            //        de.Properties[propertyName].RemoveAt(0);
            //    }
            //    else
            //    {
            //        de.Properties[propertyName][0] = propertyValue;
            //    }
            //}
            //else
            //{
            //    if (!String.IsNullOrEmpty(propertyValue))
            //    {
            //        de.Properties[propertyName].Value = propertyValue;
            //    }
            //}
            //de.CommitChanges();
            //de.Close();
        }
    }
}
