$targetFolders = "E:\Materials\Delete01","E:\Materials\Delete02"
foreach($targetFolder in $targetFolders){
    #$targetFolder = "E:\Materials\Delete01"
    $firstLevelDirectories = get-childitem $targetFolder -force -Directory
    Foreach($firstLevelDirectory in $firstLevelDirectories){
        $secondLevelDirectories = get-childitem $targetFolder\$firstLevelDirectory -force -Directory
        Foreach($secondLevelDirectory in $secondLevelDirectories){
            $miniDay = $(Get-Date).AddDays(-30).ToString('yyyyMMdd')
            if($secondLevelDirectory.ToString() -lt $miniday.ToString()){
                $dateDirectory = $targetFolder+"\"+$firstLevelDirectory+"\"+$secondLevelDirectory
                Write-Host $dateDirectory
                #输出的时候不需要加号连接，拼接的时候要用加号连接
                #Write-Host $targetFolder\$firstLevelDirectory\$secondLevelDirectory
                remove-item -LiteralPath $dateDirectory -Recurse -Force
            }
        }
    }
}