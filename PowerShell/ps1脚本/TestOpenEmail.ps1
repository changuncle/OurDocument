
do
{
    Write-Host 'Please enter numberic StartIndex'
    $startIndex=Read-Host
    $startIndex=$startIndex.Trim()
}while([String]::IsNullOrEmpty($startIndex) -or ![Regex]::IsMatch($startIndex,'^\d+$'))

do
{
    Write-Host 'Please enter numberic EndIndex'
    $endIndex=Read-Host
    $endIndex=$endIndex.Trim()
}while([String]::IsNullOrEmpty($endIndex) -or ![Regex]::IsMatch($endIndex,'^\d+$'))

if([int]$startIndex -gt [int]$endIndex){
    $startIndex=[int]$startIndex+[int]$endIndex
    $endIndex=[int]$startIndex-[int]$endIndex
    $startIndex=[int]$startIndex-[int]$endIndex
    Write-Host 'StartIndex='$startIndex
    Write-Host 'EndIndex='$endIndex
}

#Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn
$secPassword = ConvertTo-SecureString 'Helloh3c' -AsPlainText -Force
$UserCredential = New-Object System.Management.Automation.PSCredential('linktest\administrator',$secPassword)
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://test-exchange.linktest.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -DisableNameChecking
$database='DB001'
$domainController='test-dc.linktest.com'
for($i=[int]$startIndex;$i -le [int]$endIndex;$i++){
    $temp=[String]::Format('{0:d2}',$i)
    $identity='linktest\TJSX'+$temp
    $eid='JSX'+$temp
    $alias='TJSX'+$temp
    $simpleDisplayName='TJSX'+$temp
    $emailaddress1='smtp:'+$eid+'@linktest.com'
    
    Write-Host '---------['$temp']Start---------'
    Enable-Mailbox -Identity $identity -Alias $alias -Database $database -DomainController $domainController
    Set-CASMailbox -Identity $identity -ActiveSyncEnabled $false -ActiveSyncMailboxPolicy generalwithattachment -OwaMailboxPolicy general -MAPIBlockOutlookRpcHttp $true –DomainController $domainController
    Set-Mailbox -Identity $identity -SimpleDisplayName $simpleDisplayName -CustomAttribute15 'NonRDPartnerInt' –DomainController $domainController -Confirm:$false
    Set-Mailbox -Identity $identity -EmailAddresses @{Add=$emailaddress1} –DomainController $domainController
    Write-Host '---------['$temp']End-----------'
    Write-Host 
}

Write-Host 'Please press enter to exit'
$startIndex = Read-Host