﻿
param(
    [parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]
    $ServiceFolder
)
#$TargetFolder="E:\TFS-WindowsService"
$ServiceName="FirstTask"
$SourceFolder="$PSScriptRoot\*"
#please ensure there is only one exe file in source folder
$ServiceExeName=(Get-ChildItem "$PSScriptRoot\*.exe")[0].Name
$ServicePath="$ServiceFolder\$ServiceExeName"

if((Test-Path $ServiceFolder) -eq $false){
    #Write-Error "please create folder $ServiceFolder" -ErrorAction Stop    
	mkdir $ServiceFolder -Force
}

if((Get-Service -Name $ServiceName -ErrorAction Ignore) -ne $null){
    #Stop Service
    C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe $ServicePath /u
    
    do{
        #Check Service is unintall
        Write-Verbose "waiting for uninstall service $ServiceName"
        Start-Sleep -s 2
        if((Get-Service -Name $ServiceName -ErrorAction Ignore) -eq $null){
            Write-Verbose "service $ServiceName is uninstall successfully"
            break
        }
    }while($true)
}

#Copy package to Install folder
Get-ChildItem -Path $SourceFolder -Recurse -Force | Copy-Item -Destination $ServiceFolder -Force 

#install service
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe $ServicePath

#Start service
Start-Service $ServiceName